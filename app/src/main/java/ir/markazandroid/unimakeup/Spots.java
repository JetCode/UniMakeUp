package ir.markazandroid.unimakeup;

import android.graphics.PointF;
import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Coded by Ali on 9/14/2019.
 */
public class Spots {

    private static final String SpotsVertexShader =
            ""
                    + "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
                    + "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
                    + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
                    //+ "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "1;\n" //
                    + "uniform mat4 u_projTrans;\n" //
                    + "varying vec4 v_color;\n" //
                    + "varying vec2 v_texCoords;\n" //
                    //+ "varying vec2 v_texCoords1;\n" //
                    + "\n" //
                    + "void main()\n" //
                    + "{\n" //
                    + "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
                    // + "   v_color.a = v_color.a * (255.0/254.0);\n" //
                    + "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
                    // + "   v_texCoords1 = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "1;\n" //
                    + "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
                    + "}\n";

    private static final String SpotsFragmentShader =
            ""
                    + "#ifdef GL_ES\n" //
                    + "#define LOWP lowp\n" //
                    + "precision mediump float;\n" //
                    + "#else\n" //
                    + "#define LOWP \n" //
                    + "#endif\n" //
                    + "varying LOWP vec4 v_color;\n" //
                    + "varying vec2 v_texCoords;\n" //
                    // + "varying vec2 v_texCoords1;\n" //
                    + "uniform sampler2D u_texture;\n" //
                    + "uniform sampler2D u_texture1;\n"
                    + "uniform sampler2D u_texture2;\n"
                    //  + "uniform float lipStickOpacity;\n" //
                    + "void main()\n"//
                    + "{\n" //
                    // + "  vec2 cords = v_texCoords;\n"
                    // + "  cords.y = 1.0 - cords.y;\n"
                    + "  vec4 spotColor = texture2D(u_texture, v_texCoords);\n" +

                    "    float isSkin =  texture2D(u_texture1, v_texCoords).r ;\n" +

                    "    float isTouched =  texture2D(u_texture2, v_texCoords).a ;\n"

                    + "  gl_FragColor = vec4(spotColor.rgb , 2.0 * spotColor.a*isSkin*(1.0-isTouched))  ;\n" //
                    + "}";


    private Texture spots, spotsRight, spotsLeft;
    private ShaderProgram shaderProgram, defaultShaderProgram;
    private FrameBuffer buffer;


    public void init() {
        spots = new Texture(Gdx.files.internal("spots.png"));
        spotsRight = new Texture(Gdx.files.internal("spots_right.png"));
        spotsLeft = new Texture(Gdx.files.internal("spots_left.png"));

        defaultShaderProgram = SpriteBatch.createDefaultShader();

        shaderProgram = new ShaderProgram(SpotsVertexShader, SpotsFragmentShader);

        if (shaderProgram.getLog().length() != 0)
            Log.e("Shader Compiling", shaderProgram.getLog());

        shaderProgram.begin();

        shaderProgram.setUniformi("u_texture1", 1);
        shaderProgram.setUniformi("u_texture2", 2);

        shaderProgram.end();


        buffer = new FrameBuffer(Pixmap.Format.RGBA8888, Makeup.SCREEN_WIDTH, Makeup.SCREEN_HEIGHT, false);

    }

    public void draw(TextureRegion mask, Batch batch, Face face, TouchMask touchMask) {
        if (face == null || !face.hasFace())
            return;

        ShaderProgram s = batch.getShader();

        batch.setShader(defaultShaderProgram);


        buffer.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 0f); //transparent black
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        drawRight(batch, face.getPoints());
        drawLeft(batch, face.getPoints());

        batch.flush();
        buffer.end();
        TextureRegion spotsRegion = new TextureRegion(buffer.getColorBufferTexture());
        spotsRegion.flip(false, true);

        batch.setShader(shaderProgram);

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE2);
        touchMask.bind();

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE1);
        mask.getTexture().bind();

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
        spotsRegion.getTexture().bind();


        batch.draw(spotsRegion, 0, 0);

        batch.flush();

        batch.setShader(s);

    }


    private void drawLeft(Batch batch, PointF[] pointsArray) {
        float y = pointsArray[31].y;
        float x = pointsArray[2].x;
        float height = pointsArray[28].y - y;
        float width = pointsArray[31].x - x;
        batch.draw(spotsLeft, x, y, width, height);
    }

    private void drawRight(Batch batch, PointF[] pointsArray) {
        float y = pointsArray[35].y;
        float x = pointsArray[35].x;
        float height = pointsArray[28].y - y;
        float width = pointsArray[14].x - x;
        batch.draw(spotsRight, x, y, width, height);
    }


    public void dispose() {
        shaderProgram.dispose();
        spots.dispose();
        spotsLeft.dispose();
        spotsRight.dispose();
        defaultShaderProgram.dispose();
        buffer.dispose();
    }
}
