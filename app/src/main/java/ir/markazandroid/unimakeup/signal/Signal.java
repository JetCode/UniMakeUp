package ir.markazandroid.unimakeup.signal;

import java.io.Serializable;

/**
 * Coded by Ali on 29/07/2017.
 */

public class Signal implements Serializable {

    public static final int SIGNAL_MAKE_UP_CREATED = 0x00000001;
    public static final int SIGNAL_VIEW_IS_SHOWN = 0x00000010;


    private String msg;
    private int type;

    private Serializable extras;

    public Signal(String msg, int type, Serializable extras) {
        this.msg = msg;
        this.type = type;
        this.extras = extras;
    }

    public Signal(String msg, int type) {
        this.msg = msg;
        this.type = type;
    }

    public Signal(int type) {
        this.type = type;
    }

    public Signal() {
    }

    public Serializable getExtras() {
        return extras;
    }

    public void setExtras(Serializable extras) {
        this.extras = extras;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
