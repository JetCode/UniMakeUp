package ir.markazandroid.unimakeup;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Timer;
import java.util.concurrent.locks.ReentrantLock;

import ir.markazandroid.unimakeup.signal.Signal;
import ir.markazandroid.unimakeup.signal.SignalManager;
import ir.markazandroid.unimakeup.signal.SignalReceiver;

/**
 * Coded by Ali on 5/26/2019.
 */
public class CameraRenderer implements ApplicationListener, SignalReceiver {

    private final Makeup deviceCameraControl;

    private SpriteBatch spriteBatch;
    private final Context context;
    private final ReentrantLock lock;
    private volatile boolean init;

    public CameraRenderer(Makeup cameraControl, Context context) {
        this.deviceCameraControl = cameraControl;
        this.context = context;
        lock = new ReentrantLock();
    }

    @Override
    public void create() {
        Gdx.graphics.setContinuousRendering(false);
        //Gdx.graphics.requestRendering();

        Timer t = new Timer();
        Handler handler = new Handler(context.getMainLooper());
        handler.post(() -> Gdx.app.postRunnable(() -> {
            deviceCameraControl.init();
            getSignalManager().sendMainSignal(new Signal(Signal.SIGNAL_MAKE_UP_CREATED));

        }));

        getSignalManager().addReceiver(this);
        /*t.schedule(new TimerTask() {
            @Override
            public void run() {

            }
        },1);*/

        //Gdx.graphics.requestRendering();
        //spriteBatch=new SpriteBatch();
        Log.e("Width height",Gdx.graphics.getWidth()+"---"+Gdx.graphics.getHeight());

        //getSignalManager().sendMainSignal(new Signal(Signal.SIGNAL_MAKE_UP_CREATED));
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
      //  Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
      //  Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        //Render the background that is the live camera image
        //spriteBatch.begin();

        if (init)
            deviceCameraControl.drawRgba(spriteBatch);
        else {
            SpriteBatch batch = new SpriteBatch();
        }
        //    Gdx.gl.glClearColor(0.5f,0.5f,0.5f,0.5f);

        //spriteBatch.end();

        Log.e("fps",""+Gdx.graphics.getFramesPerSecond());
        /*
         * Render anything here (sprites/models etc.) that you want to go on top of the camera image
         */
    }

    @Override
    public void dispose() {
        getSignalManager().removeReceiver(this);
        deviceCameraControl.destroy();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    private SignalManager getSignalManager() {
        return ((UniMakeUpApplication) context.getApplicationContext()).getSignalManager();
    }

    @Override
    public boolean onSignal(Signal signal) {
        if (signal.getType() == Signal.SIGNAL_VIEW_IS_SHOWN)
            init = true;

        return false;
    }
}