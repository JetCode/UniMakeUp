package ir.markazandroid.unimakeup;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;

import org.opencv.core.Point;

/**
 * Coded by Ali on 9/16/2019.
 */
public class Lipsticks {


    private static final String lipSticksVertexShader =
            ""
                    + "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
                    + "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
                    + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
                    + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "1;\n" //
                    + "uniform mat4 u_projTrans;\n" //
                    + "varying vec4 v_color;\n" //
                    + "varying vec2 v_texCoords;\n" //
                    //+ "varying vec2 v_texCoords1;\n" //
                    + "\n" //
                    + "void main()\n" //
                    + "{\n" //
                    + "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
                    // + "   v_color.a = v_color.a * (255.0/254.0);\n" //
                    + "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
                    //+ "   v_texCoords1 = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "1;\n" //
                    + "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
                    + "}\n";

    private static final String lipSticksFragmentShader =
            ""
                    + "#ifdef GL_ES\n" //
                    + "#define LOWP lowp\n" //
                    + "precision mediump float;\n" //
                    + "#else\n" //
                    + "#define LOWP \n" //
                    + "#endif\n" //
                    + "varying LOWP vec4 v_color;\n" //
                    + "varying vec2 v_texCoords;\n" //
                    // + "varying vec2 v_texCoords1;\n" //
                    + "uniform sampler2D u_texture;\n" //
                    + "uniform sampler2D u_texture1;\n"
                    + "uniform float lipStickOpacity;\n" //
                    + "uniform vec4 lipstickColor;\n" //
                    + "void main()\n"//
                    + "{\n" //
                    + "  vec4 texColor = texture2D(u_texture, v_texCoords);\n"
                    // + "  vec2 cords = v_texCoords;\n"
                    // + "  cords.y = 1.0 - cords.y;\n"
                    + "  vec4 lipSticksTexColor = texture2D(u_texture1, v_texCoords);\n"

                    + "   float Y = (0.257 * texColor.r) + (0.504 * texColor.g) + (0.098 * texColor.b) + 16.0/256.0;\n"

                    + "  float a = lipSticksTexColor.a;"
                    //    + "  texColor.rgb = mix(texColor.rgb, lipSticksTexColor.rgb ,a * lipStickOpacity * Y );\n"
                    + "  gl_FragColor = vec4(lipstickColor.rgb, a * lipStickOpacity * Y) ;\n" //
                    // + "  gl_FragColor = lipStickOpacity * 0.0000001 + lipSticksTexColor*0.0000001 + texColor*0.00000001 + vec4(vec3(Y), 1.0) ;\n" //

                    + "}";

    private static final String blurFragmentShader =
            ""
                    + "#ifdef GL_ES\n" //
                    + "#define LOWP lowp\n" //
                    + "precision mediump float;\n" //
                    + "#else\n" //
                    + "#define LOWP \n" //
                    + "#endif\n" +
                    "varying vec2 v_texCoords;\n" +
                    "varying vec4 v_color;\n" +
                    "\n" +
                    "//declare uniforms\n" +
                    "uniform sampler2D u_texture;\n" +
                    "const float resolutionX=1080.0;\n" +
                    "const float resolutionY=1920.0;\n" +
                    "float radius=1.5;\n" +
                    "uniform vec2 dir;\n" +
                    "\n" +
                    "void main() {\n" +
                    "   //this will be our RGBA sum\n" +
                    "   vec4 sum = vec4(0.0);\n" +
                    "   \n" +
                    "   //our original texcoord for this fragment\n" +
                    "   vec2 tc = v_texCoords;\n" +
                    "   \n" +
                    "   //the amount to blur, i.e. how far off center to sample from \n" +
                    "   //1.0 -> blur by one pixel\n" +
                    "   //2.0 -> blur by two pixels, etc.\n" +
                    "   float blurX = radius/resolutionX; \n" +
                    "   float blurY = radius/resolutionY; \n" +
                    "    \n" +
                    "   //the direction of our blur\n" +
                    "   //(1.0, 0.0) -> x-axis blur\n" +
                    "   //(0.0, 1.0) -> y-axis blur\n" +
                    "   float hstep = dir.x;\n" +
                    "   float vstep = dir.y;\n" +
                    "    \n" +
                    "   //apply blurring, using a 9-tap filter with predefined gaussian weights\n" +
                    "    \n" +
                    "   sum += texture2D(u_texture, vec2(tc.x - 4.0*blurX*hstep, tc.y - 4.0*blurY*vstep)) * 0.0162162162;\n" +
                    "   sum += texture2D(u_texture, vec2(tc.x - 3.0*blurX*hstep, tc.y - 3.0*blurY*vstep)) * 0.0540540541;\n" +
                    "   sum += texture2D(u_texture, vec2(tc.x - 2.0*blurX*hstep, tc.y - 2.0*blurY*vstep)) * 0.1216216216;\n" +
                    "   sum += texture2D(u_texture, vec2(tc.x - 1.0*blurX*hstep, tc.y - 1.0*blurY*vstep)) * 0.1945945946;\n" +
                    "   \n" +
                    "   vec4 centerColor= texture2D(u_texture, vec2(tc.x, tc.y));\n" +
                    "   sum += centerColor * 0.2270270270;\n" +
                    "   \n" +
                    "   sum += texture2D(u_texture, vec2(tc.x + 1.0*blurX*hstep, tc.y + 1.0*blurY*vstep)) * 0.1945945946;\n" +
                    "   sum += texture2D(u_texture, vec2(tc.x + 2.0*blurX*hstep, tc.y + 2.0*blurY*vstep)) * 0.1216216216;\n" +
                    "   sum += texture2D(u_texture, vec2(tc.x + 3.0*blurX*hstep, tc.y + 3.0*blurY*vstep)) * 0.0540540541;\n" +
                    "   sum += texture2D(u_texture, vec2(tc.x + 4.0*blurX*hstep, tc.y + 4.0*blurY*vstep)) * 0.0162162162;\n" +
                    "\n" +
                    "   //discard alpha for our simple demo, multiply by vertex color and return\n" +
                    "   gl_FragColor = vec4(centerColor.rgb,sum.a);\n" +
                    "}";
    private ShaderProgram applyMakeUpProgram;
    private ShaderProgram blurShaderProgram;
    private ShaderProgram defaultShaderProgram;
    private Pixmap pix;
    private int lipStickColor = 0x9B0000FF;
    private float lipStickOpacity = 0.5f;
    private Texture textureSolid;
    private FrameBuffer makeUpFrameBuffer;
    private FrameBuffer tempFrameBuffer;
    private PolygonSprite poly;
    private TextureRegion tempRegion;
    private EarClippingTriangulator earClippingTriangulator = new EarClippingTriangulator();
    private CatmullRomSpline<Vector2> crs;


    public void init() {

        makeUpFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Makeup.SCREEN_WIDTH, Makeup.SCREEN_HEIGHT, false);
        tempFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Makeup.SCREEN_WIDTH, Makeup.SCREEN_HEIGHT, false);

        applyMakeUpProgram = new ShaderProgram(lipSticksVertexShader, lipSticksFragmentShader);

        if (applyMakeUpProgram.getLog().length() != 0)
            Log.e("Shader Compiling", applyMakeUpProgram.getLog());

        applyMakeUpProgram.begin();

        applyMakeUpProgram.setUniformi("u_texture1", 1);

        applyMakeUpProgram.end();

        blurShaderProgram = new ShaderProgram(lipSticksVertexShader, blurFragmentShader);
        //Good idea to log any warnings if they exist
        if (blurShaderProgram.getLog().length() != 0)
            Log.e("Shader Compiling", blurShaderProgram.getLog());

        blurShaderProgram.begin();
        blurShaderProgram.setUniformf("dir", 0f, 0f);
        blurShaderProgram.end();

        defaultShaderProgram = SpriteBatch.createDefaultShader();


        pix = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pix.setColor(lipStickColor);
        pix.fill();
        textureSolid = new Texture(pix);

        tempRegion = new TextureRegion(tempFrameBuffer.getColorBufferTexture());
        tempRegion.flip(false, true);

    }

    public void draw(PolygonSpriteBatch polyBatch, Point[] pointsArray, TextureRegion cameraRegion) {
        ShaderProgram s = polyBatch.getShader();

        makeUpFrameBuffer.begin();

        //Gdx.gl.glClearColor((lipStickColor>>24 & 0x00_00_00_ff)/255f, ((lipStickColor>>16) & 0x00_00_00_ff)/255f, ((lipStickColor>>8) & 0x00_00_00_FF)/255f, 0f);
        Gdx.gl.glClearColor(0, 0, 0, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        polyBatch.setShader(defaultShaderProgram);

        float[] pointsTop = new float[13 * 2];
        for (int i = 48; i < 55; i++) {
            pointsTop[(i - 48) * 2] = (float) pointsArray[i * 2].x;
            pointsTop[(i - 48) * 2 + 1] = (float) pointsArray[i * 2 + 1].x;
        }
        for (int i = 64; i > 59; i--) {
            pointsTop[(7 + 64 - i) * 2] = (float) pointsArray[i * 2].x;
            pointsTop[(7 + 64 - i) * 2 + 1] = (float) pointsArray[i * 2 + 1].x;
        }

        pointsTop[13 * 2 - 2] = pointsTop[0];
        pointsTop[13 * 2 - 1] = pointsTop[1];
        inf(polyBatch, pointsTop);

        float[] pointsBot = new float[13 * 2];
        for (int i = 54; i < 60; i++) {
            pointsBot[(i - 54) * 2] = (float) pointsArray[i * 2].x;
            pointsBot[(i - 54) * 2 + 1] = (float) pointsArray[i * 2 + 1].x;
        }
        pointsBot[6 * 2] = pointsTop[0];
        pointsBot[6 * 2 + 1] = pointsTop[1];
        pointsBot[6 * 2 + 2] = pointsTop[12 * 2 - 2];
        pointsBot[6 * 2 + 3] = pointsTop[12 * 2 - 1];

        for (int i = 67; i > 63; i--) {
            pointsBot[(8 + 67 - i) * 2] = (float) pointsArray[i * 2].x;
            pointsBot[(8 + 67 - i) * 2 + 1] = (float) pointsArray[i * 2 + 1].x;
        }
        pointsBot[13 * 2 - 2] = pointsBot[0];
        pointsBot[13 * 2 - 1] = pointsBot[1];
        inf(polyBatch, pointsBot);

        polyBatch.flush();

        makeUpFrameBuffer.end();


        //Begin Blur


        polyBatch.setShader(blurShaderProgram);

//ensure the direction is along the X-axis only
        blurShaderProgram.setUniformf("dir", 1f, 0f);

//start rendering to target B
        tempFrameBuffer.begin();

        //Gdx.gl.glClearColor((lipStickColor>>24 & 0x00_00_00_ff)/255f, ((lipStickColor>>16) & 0x00_00_00_ff)/255f, ((lipStickColor>>8) & 0x00_00_00_FF)/255f, 0f);
        //Gdx.gl.glClearColor((lipStickColor>>24 & 0x00_00_00_ff)/255f, ((lipStickColor>>16) & 0x00_00_00_ff)/255f, ((lipStickColor>>8) & 0x00_00_00_FF)/255f, 0f);
        Gdx.gl.glClearColor(0, 0, 0, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

//render target A (the scene) using our horizontal blur shader
//it will be placed into target B
        polyBatch.draw(makeUpFrameBuffer.getColorBufferTexture(), 0, 0);

//flush the batch before ending target B
        polyBatch.flush();

        tempFrameBuffer.end();

        //polyBatch.setShader(blurShaderProgram);


        blurShaderProgram.setUniformf("dir", 0f, 1f);

        makeUpFrameBuffer.begin();

        //Gdx.gl.glClearColor((lipStickColor>>24 & 0x00_00_00_ff)/255f, ((lipStickColor>>16) & 0x00_00_00_ff)/255f, ((lipStickColor>>8) & 0x00_00_00_FF)/255f, 0f);
        Gdx.gl.glClearColor(0, 0, 0, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

//update Y-axis blur radius based on mouse
        //  float mouseYAmt = (Display.getHeight()-Mouse.getY()-1) / (float)Display.getHeight();
        //   blurShader.setUniformf("radius", mouseYAmt * MAX_BLUR);

//draw the horizontally-blurred FBO target B to the screen, applying the vertical blur as we go
        polyBatch.draw(tempRegion, 0, 0);

//end of frame -- finish the batch
        polyBatch.flush();

        makeUpFrameBuffer.end();


        //Draw makeUp (lipSticks)


        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE1);
        makeUpFrameBuffer.getColorBufferTexture().bind();

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
        cameraRegion.getTexture().bind();

        polyBatch.setShader(applyMakeUpProgram);

        applyMakeUpProgram.setUniformf("lipStickOpacity", lipStickOpacity);
        applyMakeUpProgram.setUniformf("lipstickColor", (lipStickColor >> 24 & 0x00_00_00_ff) / 255f, ((lipStickColor >> 16) & 0x00_00_00_ff) / 255f, ((lipStickColor >> 8) & 0x00_00_00_FF) / 255f, 0.0f);

        polyBatch.draw(cameraRegion, 0, 0);

        polyBatch.flush();

        polyBatch.setShader(s);

    }


    void inf(PolygonSpriteBatch polyBatch, float[] points) {

        Vector2[] vectorPoints = new Vector2[points.length / 2];
        for (int i = 0; i < points.length; i += 2) {
            vectorPoints[i / 2] = new Vector2(points[i], points[i + 1]);
        }
        crs = new CatmullRomSpline<>(vectorPoints, false);

        points = new float[506];
        Vector2 temp = new Vector2();
        points[0] = vectorPoints[0].x;
        points[1] = vectorPoints[0].y;
        for (int i = 0; i < points.length / 2 - 2; i++) {
            crs.valueAt(temp, i / (float) (points.length / 2 - 3));
            points[(i + 1) * 2] = temp.x;
            points[(i + 1) * 2 + 1] = temp.y;
        }
        points[points.length - 2] = vectorPoints[vectorPoints.length - 1].x;
        points[points.length - 1] = vectorPoints[vectorPoints.length - 1].y;
// Creating the color filling (but textures would work the same way)


        PolygonRegion polyReg = new PolygonRegion(new TextureRegion(textureSolid),
                points, earClippingTriangulator.computeTriangles(points).items);
        poly = new PolygonSprite(polyReg);


        //poly.setOrigin(a, b);
        //polyBatch.begin();
        //Gdx.gl.glColorMask(false, false, false, true);

        //change the blending function for our alpha map
        //polyBatch.setBlendFunction(GL20.GL_ONE_MINUS_DST_COLOR, GL20.GLL);

        //TODO
        poly.draw(polyBatch);
        //polyBatch.end();
    }


    public void setLipStickOpacity(float lipStickOpacity) {
        this.lipStickOpacity = lipStickOpacity;
    }

    public void setLipStickColor(int color) {
        lipStickColor = color;
        if (pix != null) {
            pix.setColor(lipStickColor); // DE is red, AD is green and BE is blue.
            pix.fill();
            textureSolid.draw(pix, 0, 0);
        }
    }

    public void dispose() {
        applyMakeUpProgram.dispose();
        blurShaderProgram.dispose();
        defaultShaderProgram.dispose();
        pix.dispose();
        textureSolid.dispose();
        makeUpFrameBuffer.dispose();
        tempFrameBuffer.dispose();
    }
}
