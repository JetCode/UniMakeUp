package ir.markazandroid.unimakeup;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Coded by Ali on 7/29/2019.
 */
public class BilFilter {


    public static final String BILATERAL_VERTEX_SHADER = "" +
            "attribute vec4 a_position;\n" +
            "attribute vec2 a_texCoord0;\n" +

            "const int GAUSSIAN_SAMPLES = 18;\n" +


            "varying vec2 textureCoordinate;\n" +
            "varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +

            "uniform mat4 u_projTrans;\n" +

            "uniform vec2 singleStepOffset;\n" +

            "void main()\n" +
            "{\n" +
            "	gl_Position = u_projTrans * a_position;\n" +
            "	textureCoordinate = a_texCoord0;\n" +

            "	int multiplier = 0;\n" +
            "	vec2 blurStep;\n" +

            "	for (int i = 0; i < GAUSSIAN_SAMPLES/2; i++)\n" +
            "	{\n" +
            "		multiplier = (i - ((GAUSSIAN_SAMPLES/2 - 1) / 2));\n" +

            "		blurStep = float(multiplier) * singleStepOffset;\n" +
            "		blurCoordinates[i].x = a_texCoord0.x + blurStep.x;\n" +
            "		blurCoordinates[i].y = a_texCoord0.y;\n" +
            "	}\n" +
            "	for (int j = 0; j < GAUSSIAN_SAMPLES/2; j++)\n" +
            "	{\n" +
            "		multiplier = (j - ((GAUSSIAN_SAMPLES/2 - 1) / 2));\n" +

            "		blurStep = float(multiplier) * singleStepOffset;\n" +
            "		blurCoordinates[j+(GAUSSIAN_SAMPLES/2)].x = a_texCoord0.x;\n" +
            "		blurCoordinates[j+(GAUSSIAN_SAMPLES/2)].y = a_texCoord0.y + blurStep.y;\n" +
            "	}\n" +
            "}";

    public static final String BILATERAL_FRAGMENT_SHADER = "" +
            "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" +

            "uniform sampler2D u_texture;\n" +

            "uniform sampler2D mask;\n" +

            "uniform sampler2D touchMask;\n" +

            "uniform vec4 creamColor;\n" +

            " const int GAUSSIAN_SAMPLES = 18;\n" +

            " varying vec2 textureCoordinate;\n" +
            " varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +

            " uniform float distanceNormalizationFactor;\n" +

            " uniform float creamOpacity;\n" +

            " void main()\n" +
            " {\n" +
            "       vec4 centralColor;\n" +
            "       float gaussianWeightTotal;\n" +
            "      vec4 sum;\n" +
            "       vec4 sampleColor;\n" +
            "       float distanceFromCentralColor;\n" +
            "      float gaussianWeight;\n" +
            "     \n" +
            "     centralColor = texture2D(u_texture, blurCoordinates[4]);\n" +
            "     gaussianWeightTotal = 0.2270270270;\n" +
            "     sum = centralColor * 0.2270270270;\n" +
            "     \n" +
            // X
            "     sampleColor = texture2D(u_texture, blurCoordinates[0]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[1]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[2]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[3]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[5]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[6]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[7]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[8]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +
            //Y
            "     sampleColor = texture2D(u_texture, blurCoordinates[9]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[10]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[11]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[12]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[14]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[15]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[16]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[17]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     vec4 sum1 = sum / gaussianWeightTotal;" +

//            "      vec4 thr = vec4(70.0/255.0, 125.0/255.0, 130.0/255.0, 180.0/255.0);"+

            "     float Y = (0.257 * sum1.r) + (0.504 * sum1.g) + (0.098 * sum1.b) + 16.0/256.0;\n" +
            "     float V = (0.439 *  sum1.r) - (0.368 *  sum1.g) - (0.071 * sum1.b) + 128.0/256.0;\n" +
            "     float U = -(0.148 *  sum1.r) - (0.291 *  sum1.g) + (0.439 * sum1.b) + 128.0/256.0;\n"+

//            "     float uPL = smoothstep(thr[0],thr[1],U);\n"+
            //           "     float uP = ((1.0-uPL) * uPL )* 4.0;\n"+

//            "     float vPL = smoothstep(thr[2],thr[3],V);\n"+
//            "     float vP = ((1.0-vPL) * vPL )* 4.0;\n"+

//            "     float isSkin = ((uP + vP) /2.0) * step(thr[0], Y) ;\n"+

            "     Y = Y*1.2;\n"+

           // "     U = U*1.2;\n"+

          //  "     V = V*1.2;\n"+


            //"     float vP = smoothstep(thr[2],thr[3],V);\n"+


           // "     float vP = smoothstep(thr[2],thr[3],V);\n"+



          //  "      Y =  (Y > 73.0/255.0) &&  \n" +
          //    "      (U >= thr[0]) && (U <= thr[1]) && \n" +
          //    "      (V >= thr[2]) && (V <=thr[3]) ? 1.0 : 0.0; \n" +

            "   float r, g, b, a;                         \n" +

            " Y = 1.1643 * ( Y - 0.0625 );\n" +

            "\n" +
            "            U = U - 0.5;\n" +
            "            V = V - 0.5;\n" +
            "\n" +
            "            r = Y + 1.5958 * V;\n" +
            "            g = Y - 0.39173 * U - 0.81290 * V;\n" +
            "            b = Y + 2.017 * U;\n" +
            "\n" +
            "     float isSkin = texture2D(mask, textureCoordinate).r;" +

            "     float isTouched = texture2D(touchMask, textureCoordinate).a;" +
       //     "            sum1 = vec4(r,g,b, 1.0);"

            "   sum1 = mix(sum1,creamColor,isSkin*creamOpacity);                              \n" +


         //   "      sum1 = vec4(vec3(( (Y > thr[0]) && \n" +
          //  "      (U >= thr[0]) && (U <= thr[1]) && \n" +
          //  "      (V >= thr[2]) && (V <=thr[3])) ? 1.0 : 0.0), \n" +
         //  "      1.0);"+
          //  "     sum1 = mix(sum1,sum1* vec4(0.9,0.9,0.9,1)=,0.6);"+
            //        "     vec4 maskColor = texture2D(mask, textureCoordinate);"+

            //     "     gl_FragColor =vec4(vec3(isSkin),1.0) + sum1 * 0.000001 ;\n" +

            //    "     gl_FragColor = vec4(sum1.rgb,isTouched * isSkin);\n" +


            "     gl_FragColor = isSkin == 0.0? vec4(0.0) : vec4(sum1.rgb,isTouched);\n" +
            " }";

    public static final String BILATERAL_FRAGMENT_SHADER0 = "" +
            "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" +

            "uniform sampler2D u_texture;\n" +

            "uniform sampler2D mask;\n" +

            " const int GAUSSIAN_SAMPLES = 9;\n" +

            " varying vec2 textureCoordinate;\n" +
            " varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +

            " uniform float distanceNormalizationFactor;\n" +

            " void main()\n" +
            " {\n" +
            "      vec4 centralColor;\n" +
            "      float gaussianWeightTotal;\n" +
            "      vec4 sum;\n" +
            "      vec4 sampleColor;\n" +
            "      float distanceFromCentralColor;\n" +
            "      float gaussianWeight;\n" +
            "     \n" +
            "     centralColor = texture2D(u_texture, blurCoordinates[4]);\n" +
            "     gaussianWeightTotal = 0.18;\n" +
            "     sum = centralColor * 0.18;\n" +
            "     \n" +
            "     sampleColor = texture2D(u_texture, blurCoordinates[0]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.05 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[1]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.09 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[2]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.12 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[3]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.15 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[5]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.15 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[6]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.12 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[7]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.09 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[8]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.05 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +
            "     vec4 sum1 = sum / gaussianWeightTotal;" +
            "     sum1 = mix(sum1,vec(0.9,0.9,0.9,1),0.6);"+
            "     gl_FragColor = texture2D(mask, textureCoordinate).r == 0.0? sum1 : sampleColor;\n" +
            " }";

    public static final String BILATERAL_FRAGMENT_SHADER1 = "" +
            "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" +

            "uniform sampler2D u_texture;\n" +

            " varying vec2 textureCoordinate;\n" +
            " varying vec2 blurCoordinates[9];\n" +

            " uniform float distanceNormalizationFactor;\n" +

            " void main()\n" +
            " {\n" +
            "     gl_FragColor = vec4(texture2D(u_texture, blurCoordinates[8]).xyz,distanceNormalizationFactor);\n" +
            " }";

    private float distanceNormalizationFactor;
    private int disFactorLocation;
    private float[] singleStepOffsetLocation;

    private ShaderProgram bilShaderProgram;

    private float creamOpacity = 0/*0.15f*/;
    private int creamColor; // vec4(180.0/255.0,55.0/255.0,0.0/255.0,1.0)


    public void init() {
        bilShaderProgram = new ShaderProgram(BilFilter.BILATERAL_VERTEX_SHADER, BilFilter.BILATERAL_FRAGMENT_SHADER);
        if (bilShaderProgram.getLog().length() != 0)
            Log.e("Shader Compiling", bilShaderProgram.getLog());

        bilShaderProgram.begin();
        bilShaderProgram.setUniformi("mask", 1);
        bilShaderProgram.setUniformi("touchMask", 2);
        bilShaderProgram.end();

        //blurFrameBuffer2 = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);

    }


    public void doBlur(Batch batch, TextureRegion toBlur, TextureRegion mask, TouchMask touchMask) {

        ShaderProgram defaultShaderProgram = batch.getShader();

        batch.setShader(bilShaderProgram);

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE2);
        touchMask.bind();

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE1);
        mask.getTexture().bind();

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
        toBlur.getTexture().bind();


        bilShaderProgram.setUniformf("distanceNormalizationFactor", 4f);
        bilShaderProgram.setUniformf("creamOpacity", creamOpacity);
        bilShaderProgram.setUniformf("creamColor", (creamColor >> 24 & 0x00_00_00_ff) / 255f, ((creamColor >> 16) & 0x00_00_00_ff) / 255f, ((creamColor >> 8) & 0x00_00_00_FF) / 255f, 0.0f);


        // tempFrameBuffer.begin();
        //drawX(batch,toBlur);
        //tempFrameBuffer.end();

        //TextureRegion bluredX = new TextureRegion(tempFrameBuffer.getColorBufferTexture());
        //bluredX.flip(false, true);

        //batch.setShader(defaultShaderProgram);

        drawXY(batch,toBlur);

        batch.setShader(defaultShaderProgram);

        /*blurFrameBuffer2.begin();
        drawY(batch,toBlur);
        blurFrameBuffer2.end();

        TextureRegion bluredXY = new TextureRegion(blurFrameBuffer2.getColorBufferTexture());
        bluredXY.flip(false, true);

        drawXY(batch,bluredXY);*/

        batch.setShader(defaultShaderProgram);

    }



    private void drawX(Batch batch, TextureRegion toBlur) {
        singleStepOffsetLocation = new float[]{1.0f / 100, 1.0f / 1920};
        draw(batch, toBlur);

    }

    private void drawY(Batch batch, TextureRegion toBlur) {
        singleStepOffsetLocation = new float[]{0f,0};
        draw(batch, toBlur);

    }

    private void drawXY(Batch batch, TextureRegion toBlur) {
        singleStepOffsetLocation = new float[]{12.0f / 1080,12.0f / 1920};
        draw(batch, toBlur);

    }

    private void draw(Batch batch, TextureRegion toBlur) {

        //Gdx.gl.glTexParameterf(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MIN_FILTER, GL20.GL_NEAREST);
        //Gdx.gl.glTexParameterf(GL20.GL_TEXTURE_2D, GL20.GL_TEXTURE_MAG_FILTER, GL20.GL_NEAREST);

      /*  Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));*/

        bilShaderProgram.setUniformf("singleStepOffset", singleStepOffsetLocation[0], singleStepOffsetLocation[1]);

        batch.draw(toBlur, 0, 0);

        batch.flush();

    }

    public float getCreamOpacity() {
        return creamOpacity;
    }

    public void setCreamOpacity(float creamOpacity) {
        this.creamOpacity = creamOpacity;
    }


    public void dispose() {
        bilShaderProgram.dispose();
    }


    public int getCreamColor() {
        return creamColor;
    }

    public void setCreamColor(int creamColor) {
        this.creamColor = creamColor;
    }
}
