package ir.markazandroid.unimakeup.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ir.markazandroid.unimakeup.R;

import static ir.markazandroid.unimakeup.util.Utils.getColorInt;

/**
 * Coded by Ali on 12/14/2019.
 */
public class MakeupTool implements Serializable {

    public static final String BRAND_BLACK_DIAMOND = "BLACK DIAMOND";
    public static final String BRAND_SCHON = "SCHON";
    public static final String BRAND_NOTE = "NOTE";
    public static final String BRAND_CALLISTA = "CALLISTA";

    private int color;
    private int logo;
    private String name;
    private int brandLogo;
    private String code;
    private String brandName;

    public MakeupTool() {
    }

    public MakeupTool(int color, int logoId, String name, int brandLogo, String code, String brandName) {
        this.color = color;
        this.logo = logoId;
        this.name = name;
        this.brandLogo = brandLogo;
        this.code = code;
        this.brandName = brandName;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(int brandLogo) {
        this.brandLogo = brandLogo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MakeupTool)) return false;

        MakeupTool that = (MakeupTool) o;

        if (color != that.color) return false;
        if (logo != that.logo) return false;
        if (brandLogo != that.brandLogo) return false;
        if (!name.equals(that.name)) return false;
        return code.equals(that.code);
    }

    @Override
    public int hashCode() {
        int result = color;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + brandLogo;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        return result;
    }

    public static List<MakeupTool> getLipsticks() {
        ArrayList<MakeupTool> list = new ArrayList<>();
        //Black diamond
        list.add(new MakeupTool(getColorInt(212, 126, 107), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "11", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(189, 107, 111), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "12", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(200, 80, 91), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "13", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(184, 103, 125), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "14", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(165, 88, 108), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "15", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(197, 57, 76), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "16", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(175, 39, 69), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "17", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(162, 81, 86), R.mipmap.lipstick_black_diamond_c1_t, "SEMI MATT", R.drawable.blackdiamond_brand_logo, "18", BRAND_BLACK_DIAMOND));

        //Callista
        list.add(new MakeupTool(getColorInt(203, 71, 108), R.mipmap.galmor_shine_callista, "GALMOR SHINE", R.drawable.callista_brand_logo, "S81", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(196, 82, 29), R.mipmap.galmor_shine_callista, "GALMOR SHINE", R.drawable.callista_brand_logo, "S82", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(193, 36, 43), R.mipmap.galmor_shine_callista, "GALMOR SHINE", R.drawable.callista_brand_logo, "S84", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(129, 28, 25), R.mipmap.galmor_shine_callista, "GALMOR SHINE", R.drawable.callista_brand_logo, "S86", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(125, 59, 35), R.mipmap.galmor_shine_callista, "GALMOR SHINE", R.drawable.callista_brand_logo, "S88", BRAND_CALLISTA));

        list.add(new MakeupTool(getColorInt(107, 39, 22), R.mipmap.color_rich_callista, "COLOR RICH", R.drawable.callista_brand_logo, "C21", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(181, 95, 92), R.mipmap.color_rich_callista, "COLOR RICH", R.drawable.callista_brand_logo, "C22", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(160, 58, 30), R.mipmap.color_rich_callista, "COLOR RICH", R.drawable.callista_brand_logo, "C23", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(135, 35, 55), R.mipmap.color_rich_callista, "COLOR RICH", R.drawable.callista_brand_logo, "C24", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(192, 15, 38), R.mipmap.color_rich_callista, "COLOR RICH", R.drawable.callista_brand_logo, "C32", BRAND_CALLISTA));

        //Schon
        list.add(new MakeupTool(getColorInt(217, 139, 173), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A11", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(189, 80, 94), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A21", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(210, 112, 91), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A22", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(199, 122, 73), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A33", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(174, 28, 31), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A51", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(135, 31, 29), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A52", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(98, 34, 22), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A53", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(104, 32, 40), R.mipmap.schon_aqua_charming, "AQUA CHARMING", R.drawable.schon_brand_logo, "A61", BRAND_SCHON));

        return list;

    }

    public static List<MakeupTool> getFoundations() {
        ArrayList<MakeupTool> list = new ArrayList<>();
        list.add(new MakeupTool(getColorInt(224, 189, 177), R.mipmap.foundation_black_diamond, "VELVET MAKEUP", R.drawable.blackdiamond_brand_logo, "FS 01", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(214, 174, 159), R.mipmap.foundation_black_diamond, "VELVET MAKEUP", R.drawable.blackdiamond_brand_logo, "FS 02", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(218, 174, 142), R.mipmap.foundation_black_diamond, "VELVET MAKEUP", R.drawable.blackdiamond_brand_logo, "FS 03", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(221, 179, 147), R.mipmap.foundation_black_diamond, "VELVET MAKEUP", R.drawable.blackdiamond_brand_logo, "FS 04", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(211, 165, 139), R.mipmap.foundation_black_diamond, "VELVET MAKEUP", R.drawable.blackdiamond_brand_logo, "FS 05", BRAND_BLACK_DIAMOND));
        list.add(new MakeupTool(getColorInt(204, 158, 127), R.mipmap.foundation_black_diamond, "VELVET MAKEUP", R.drawable.blackdiamond_brand_logo, "FS 06", BRAND_BLACK_DIAMOND));

        //Callista
        list.add(new MakeupTool(getColorInt(199, 126, 73), R.mipmap.long_lasting_powder_callista, "LONG LASTING", R.drawable.callista_brand_logo, "M16", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(218, 161, 101), R.mipmap.long_lasting_powder_callista, "LONG LASTING", R.drawable.callista_brand_logo, "M15", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(211, 161, 114), R.mipmap.long_lasting_powder_callista, "LONG LASTING", R.drawable.callista_brand_logo, "M14", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(227, 181, 114), R.mipmap.long_lasting_powder_callista, "LONG LASTING", R.drawable.callista_brand_logo, "M13", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(234, 188, 128), R.mipmap.long_lasting_powder_callista, "LONG LASTING", R.drawable.callista_brand_logo, "M12", BRAND_CALLISTA));
        list.add(new MakeupTool(getColorInt(242, 210, 155), R.mipmap.long_lasting_powder_callista, "LONG LASTING", R.drawable.callista_brand_logo, "M11", BRAND_CALLISTA));

        //Schon
        list.add(new MakeupTool(getColorInt(244, 223, 207), R.mipmap.schon_foundation_precious, "PRECIOUS", R.drawable.schon_brand_logo, "P01", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(238, 203, 177), R.mipmap.schon_foundation_precious, "PRECIOUS", R.drawable.schon_brand_logo, "P02", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(241, 211, 163), R.mipmap.schon_foundation_precious, "PRECIOUS", R.drawable.schon_brand_logo, "P03", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(236, 195, 166), R.mipmap.schon_foundation_precious, "PRECIOUS", R.drawable.schon_brand_logo, "P04", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(229, 194, 154), R.mipmap.schon_foundation_precious, "PRECIOUS", R.drawable.schon_brand_logo, "P05", BRAND_SCHON));
        list.add(new MakeupTool(getColorInt(212, 170, 124), R.mipmap.schon_foundation_precious, "PRECIOUS", R.drawable.schon_brand_logo, "P06", BRAND_SCHON));


        return list;

    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }
}
