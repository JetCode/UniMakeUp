package ir.markazandroid.unimakeup;

import android.graphics.PointF;
import android.graphics.RectF;

import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.util.Arrays;

/**
 * Coded by Ali on 9/15/2019.
 */
public class Face {

    private PointF[] points = new PointF[68];
    private RectF faceRect;
    private PointF center;
    private PointF[] meters;
    private boolean hasFace;


    public void setPoints(Point[] pointsArray, Rect fr) {
        if (fr == null || fr.height == 0 || fr.width == 0) {
            faceRect = null;
            center = null;
            meters = null;
            points = null;
            hasFace = false;

        } else {
            if (points == null)
                points = new PointF[68];

            for (int i = 0; i < 68; i++) {
                points[i] = new PointF((float) pointsArray[i * 2].x, (float) pointsArray[i * 2 + 1].x);
            }
            this.faceRect = new RectF((float) fr.tl().x, (float) fr.tl().y * 1.2f /* add 20% to the top border of rect to have larger area */,
                    (float) fr.br().x, (float) fr.br().y);

            center = points[30];

            if (meters == null)
                meters = new PointF[4];

            meters[0] = new PointF(points[15].x - center.x, faceRect.top - center.y);
            meters[1] = new PointF(center.x - points[1].x, faceRect.top - center.y);

            meters[2] = new PointF(center.x - points[1].x, center.y - points[8].y);
            meters[3] = new PointF(points[15].x - center.x, center.y - points[8].y);

            hasFace = true;
        }
    }


    public PointF[] getLeftEyeArea() {
        return Arrays.copyOfRange(points, 36, 42);
    }

    public PointF[] getRightEyeArea() {
        return Arrays.copyOfRange(points, 42, 48);
    }

    public PointF[] getMouthArea() {
        return Arrays.copyOfRange(points, 48, 60);
    }

    public PointF[] getFaceArea() {

        PointF[] point = new PointF[19];

        float fx = points[0].x;
        float fy = faceRect.top;

        float lx = points[16].x;
        float ly = faceRect.top;

        point[0] = new PointF(fx, fy);

        System.arraycopy(points, 0, point, 1, 17);

        point[18] = new PointF(lx, ly);

        return point;
    }


    public PointF obtainPoint(PointF selectedPoint) {
        float x = selectedPoint.x - center.x;
        float y = selectedPoint.y - center.y;

        if (x > 0 && y > 0) {
            return new PointF(x / meters[0].x, y / meters[0].y);
        } else if (x < 0 && y > 0) {
            return new PointF(x / meters[1].x, y / meters[1].y);
        } else if (x < 0 && y < 0) {
            return new PointF(x / meters[2].x, y / meters[2].y);
        } else {
            return new PointF(x / meters[3].x, y / meters[3].y);
        }
    }

    public PointF matchPoint(PointF obtainedPoint) {
        float x, y;
        if (obtainedPoint.x > 0 && obtainedPoint.y > 0) {
            x = obtainedPoint.x * meters[0].x;
            y = obtainedPoint.y * meters[0].y;
        } else if (obtainedPoint.x < 0 && obtainedPoint.y > 0) {
            x = obtainedPoint.x * meters[1].x;
            y = obtainedPoint.y * meters[1].y;
        } else if (obtainedPoint.x < 0 && obtainedPoint.y < 0) {
            x = obtainedPoint.x * meters[2].x;
            y = obtainedPoint.y * meters[2].y;
        } else {
            x = obtainedPoint.x * meters[3].x;
            y = obtainedPoint.y * meters[3].y;
        }

        return new PointF(center.x + x, center.y + y);
    }

    public PointF[] getPoints() {
        return points;
    }

    public boolean hasFace() {
        return hasFace;
    }
}
