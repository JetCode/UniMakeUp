package ir.markazandroid.unimakeup.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.view.View;

/**
 * Coded by Ali on 9/8/2019.
 */
public class Utils {

    public static int getColorInt(int r, int g, int b) {
        return Color.argb(255, r, g, b);
    }


    public static int convertARBG2RGBA(int argb) {
        int a = (argb >> 24) & 0x00_00_00_ff;
        //9505019
        return (argb << 8) | a;
    }

    public static void fadeFade(final View toFade, int duration, boolean gone) {
        toFade.animate().alpha(0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        toFade.setVisibility(gone ? View.GONE : View.INVISIBLE);
                    }
                });

    }

    public static void fade(View toVisibale, final View toFade, int duration) {
        toVisibale.setAlpha(0f);
        toVisibale.setVisibility(View.VISIBLE);
        toVisibale.animate().alpha(1f)
                .setDuration(duration)
                .setListener(null);
        toFade.setAlpha(1f);
        toFade.animate().alpha(0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        toFade.setVisibility(View.GONE);
                    }
                });

    }
}
