package ir.markazandroid.unimakeup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ir.markazandroid.unimakeup.util.Utils;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StarterActivity extends AppCompatActivity {

    private View scenarioButton, makeupCameraButton, makeupCameraButtonTv, makeupCameraButtonPb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);

        scenarioButton = findViewById(R.id.scenarioButton);
        makeupCameraButton = findViewById(R.id.makeupCameraButton);
        makeupCameraButtonTv = findViewById(R.id.makeupCameraButtonTv);
        makeupCameraButtonPb = findViewById(R.id.makeupCameraButtonPb);

        makeupCameraButton.setOnClickListener(v -> {
            Utils.fade(makeupCameraButtonPb, makeupCameraButtonTv, 500);
            makeupCameraButton.setClickable(false);
            startCamera(CameraViewActivity.ACTION_MAKEUP_CAMERA);
            finish();
        });
        scenarioButton.setOnClickListener(v -> startCamera(CameraViewActivity.ACTION_SCENARIO));

    }

    private void startCamera(String actionMakeupCamera) {
        Intent intent = new Intent(this, CameraViewActivity.class);
        intent.setAction(actionMakeupCamera);
        startActivity(intent);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
