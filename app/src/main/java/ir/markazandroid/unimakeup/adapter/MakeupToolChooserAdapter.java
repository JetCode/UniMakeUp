package ir.markazandroid.unimakeup.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ir.markazandroid.unimakeup.R;
import ir.markazandroid.unimakeup.object.MakeupTool;


/**
 * Coded by Ali on 3/5/2019.
 */
public class MakeupToolChooserAdapter extends RecyclerView.Adapter<MakeupToolChooserAdapter.ViewHolder> {


    public static final int TYPE_CLEAR = 1;
    public static final int TYPE_COLOR = 2;

    public interface ColorClickListener {
        boolean onToolClick(MakeupTool color);
    }

    private List<MakeupTool> tools;
    private Context context;
    private ColorClickListener listener;
    private MakeupTool selectedTool;

    public MakeupToolChooserAdapter(Context context, ColorClickListener listener) {
        this.context = context;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_COLOR)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.entry_lipstick_color, parent, false));
        else
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.entry_clear, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_COLOR) {
            MakeupTool tool = tools.get(position - 1);
            holder.image.setImageDrawable(new ColorDrawable(tool.getColor()));

            holder.label.setText(tool.getCode());

            if (tool.equals(selectedTool))
                holder.transition.startTransition(0);
            else
                holder.transition.resetTransition();


            holder.itemView.setOnClickListener(v -> {
                if (!tool.equals(selectedTool)) {
                    onClick(tool, holder);
                }
            });
        } else if (getItemViewType(position) == TYPE_CLEAR) {
            if (selectedTool == null)
                holder.transition.startTransition(0);
            else
                holder.transition.resetTransition();

            holder.itemView.setOnClickListener(v -> {
                if (selectedTool != null) {
                    onClick(null, holder);
                }
            });
        }
    }

    private void onClick(MakeupTool tool, ViewHolder holder) {
        int lastIndex = tools.indexOf(selectedTool);
        selectedTool = tool;
        holder.transition.startTransition(500);
        notifyItemChanged(lastIndex + 1);
        listener.onToolClick(tool);
    }

    @Override
    public int getItemCount() {
        return (tools == null ? 0 : tools.size()) + 1;
    }

    public void setTools(List<MakeupTool> tools) {
        this.tools = tools;
        notifyDataSetChanged();
    }

    public MakeupTool getSelectedTool() {
        return selectedTool;
    }

    public void setSelectedTool(MakeupTool selectedTool) {
        this.selectedTool = selectedTool;
        notifyDataSetChanged();
    }

    public void refresh() {
        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView label;
        private TransitionDrawable transition;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            label = itemView.findViewById(R.id.label);
            transition = (TransitionDrawable) itemView.getBackground();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_CLEAR;
        else return TYPE_COLOR;

    }

    public List<MakeupTool> getTools() {
        return tools;
    }
}
