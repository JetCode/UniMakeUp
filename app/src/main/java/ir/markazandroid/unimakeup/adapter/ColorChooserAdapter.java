package ir.markazandroid.unimakeup.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import ir.markazandroid.unimakeup.R;


/**
 * Coded by Ali on 3/5/2019.
 */
public class ColorChooserAdapter extends RecyclerView.Adapter<ColorChooserAdapter.ViewHolder> {

    public interface ColorClickListener {
        boolean onColorClick(int color);
    }

    private List<Integer> colors;
    private Context context;
    private ColorClickListener listener;
    private int selectedColor;

    public ColorChooserAdapter(Context context, ColorClickListener listener) {
        this.context = context;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.entry_lipstick_color, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int color = colors.get(position);
        holder.image.setImageDrawable(new ColorDrawable(color));

        if (color == selectedColor)
            holder.transition.startTransition(0);
        else
            holder.transition.resetTransition();


        holder.itemView.setOnClickListener(v -> {
            if (color != selectedColor) {
                int lastIndex = colors.indexOf(selectedColor);
                selectedColor = color;
                holder.transition.startTransition(500);
                notifyItemChanged(lastIndex);
                listener.onColorClick(color);
            }
        });
    }


    @Override
    public int getItemCount() {
        return colors == null ? 0 : colors.size();
    }

    public void setColors(List<Integer> colors) {
        this.colors = colors;
        notifyDataSetChanged();
    }

    public int getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
        notifyDataSetChanged();
    }

    public void refresh() {
        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TransitionDrawable transition;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            transition = (TransitionDrawable) itemView.getBackground();
        }
    }
}
