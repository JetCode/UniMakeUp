package ir.markazandroid.unimakeup;

import android.app.Application;

import ir.markazandroid.unimakeup.signal.SignalManager;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Coded by Ali on 12/14/2019.
 */
public class UniMakeUpApplication extends Application {

    private SignalManager signalManager;

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.Builder config = new CalligraphyConfig.Builder()
                .setFontAttrId(R.attr.fontPath);

        // if (!Locale.getDefault().getLanguage().equals("en")){
        config.setDefaultFontPath("fonts/BTitrBd.ttf");
        // }

        CalligraphyConfig.initDefault(config.build());
    }

    public SignalManager getSignalManager() {
        if (signalManager == null) signalManager = new SignalManager(this);
        return signalManager;
    }
}
