package ir.markazandroid.unimakeup;

import android.graphics.PointF;
import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

/**
 * Coded by Ali on 9/16/2019.
 */
public class TouchMask {

    private DrawablePixmap drawable;
    private DrawingInput drawingInput;
    private ShaderProgram defaultShaderProgram;
    private Texture fullTouchedTexture;
    private boolean fullTouched;

    public void init() {

        drawable = new DrawablePixmap(new Pixmap(Makeup.SCREEN_WIDTH,
                Makeup.SCREEN_HEIGHT, Pixmap.Format.Alpha), 2);

        Pixmap pixmap = new Pixmap(Makeup.SCREEN_WIDTH,
                Makeup.SCREEN_HEIGHT, Pixmap.Format.Alpha);
        pixmap.setColor(drawable.drawColor);
        pixmap.fill();
        fullTouchedTexture = new Texture(pixmap);


        Gdx.input.setInputProcessor(drawingInput = new DrawingInput());


    }


    public void update(Face face) {

        if (fullTouched || face == null || !face.hasFace())
            return;

        drawingInput.setFace(face);
        drawable.clear();

        ArrayList<PointF> pointsToDraw = drawingInput.getPoints();
        Log.e("Points TD:", pointsToDraw.size() + "");
        for (int i = 0; i < pointsToDraw.size(); i++) {
            PointF pointF = pointsToDraw.get(i);
            // if (i ==0)
            drawable.draw(new Vector2(pointF.x, pointF.y));
            // else
            //     drawable.drawLerped(pointsToDraw.get(i-1),pointF);
        }
        drawable.update();

    }

    public void bind() {

        if (fullTouched)
            fullTouchedTexture.bind();
        else
            drawable.texture.bind();
    }

    public boolean isFullTouched() {
        return fullTouched;
    }

    public void setFullTouched(boolean fullTouched) {
        this.fullTouched = fullTouched;
    }


    private static class DrawingInput extends InputAdapter {

        private PointF last = null;
        private boolean leftDown = false;

        private ArrayList<PointF> touchedPoints;
        private Face face;

        public DrawingInput() {
            touchedPoints = new ArrayList<>();
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer,
                                 int button) {
            if (button == Input.Buttons.LEFT) {
                last = savePoint(screenX, screenY);
                leftDown = true;
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if (leftDown) {
                last = savePoint(screenX, screenY);
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (button == Input.Buttons.LEFT) {
                savePoint(screenX, screenY);
                last = null;
                leftDown = false;
                return true;
            } else {
                return false;
            }
        }


        public float dst(PointF from, PointF to) {
            final float x_d = to.x - from.x;
            final float y_d = to.y - from.y;
            return (float) Math.sqrt(x_d * x_d + y_d * y_d);
        }

        private PointF savePoint(int x, int y) {
            if (face != null) {
                PointF touchedPoint = face.obtainPoint(new PointF(x, Makeup.SCREEN_HEIGHT - y));
                touchedPoints.add(touchedPoint);
                return touchedPoint;
            } else return null;
        }

        public ArrayList<PointF> getPoints() {
            ArrayList<PointF> points = new ArrayList<>();
            if (face != null) {
                for (PointF obtainedPoint : touchedPoints) {
                    points.add(face.matchPoint(obtainedPoint));
                }
            }
            return points;
        }

        public void setFace(Face face) {
            this.face = face;
        }
    }


    /**
     * Nested (static) class to provide a nice abstraction over Pixmap, exposing
     * only the draw calls we want and handling some of the logic for smoothed
     * (linear interpolated, aka 'lerped') drawing. This will become the 'owner'
     * of the underlying pixmap, so it will need to be disposed.
     */
    private static class DrawablePixmap implements Disposable {

        private final int brushSize = 30;
        private final Color clearColor = new Color(0, 0, 0, 0);
        private final Color drawColor = new Color(1, 1, 1, 1);

        private Pixmap pixmap;
        private Texture texture;

        public DrawablePixmap(Pixmap pixmap, int textureBinding) {
            this.pixmap = pixmap;
            pixmap.setColor(drawColor);

            /* Create a texture which we'll update from the pixmap. */
            this.texture = new Texture(pixmap);

            /* Bind the mask texture to TEXTURE<N> (TEXTURE1 for our purposes),
             * which also sets the currently active texture unit. */
            this.texture.bind(textureBinding);

            /* However SpriteBatch will auto-bind to the current active texture,
             * so we must now reset it to TEXTURE0 or else our mask will be
             * overwritten. */
            Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
        }

        /**
         * Write the pixmap onto the texture if the pixmap has changed.
         */
        public void update() {
            texture.draw(pixmap, 0, 0);
        }

        public void clear() {
            pixmap.setColor(clearColor);
            pixmap.fill();
            pixmap.setColor(drawColor);
        }


        public void draw(Vector2 spot) {
            pixmap.fillCircle((int) spot.x, (int) (Makeup.SCREEN_HEIGHT - spot.y), brushSize);
        }

        public void drawLerped(PointF fromF, PointF toF) {
            Vector2 from = new Vector2(fromF.x, fromF.y);
            Vector2 to = new Vector2(toF.x, toF.y);
            float dist = to.dst(from);
            /* Calc an alpha step to put one dot roughly every 1/8 of the brush
             * radius. 1/8 is arbitrary, but the results are fairly nice. */
            float alphaStep = brushSize / (2f * dist);

            for (float a = 0; a < 1f; a += alphaStep) {
                Vector2 lerped = from.lerp(to, a);
                draw(lerped);
            }

            draw(to);
        }

        @Override
        public void dispose() {
            texture.dispose();
            pixmap.dispose();
        }
    }

    public void dispose() {
        drawable.dispose();
        Gdx.input.setInputProcessor(null);
    }
}
