package ir.markazandroid.unimakeup.fragment;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SeekBar;

import ir.markazandroid.unimakeup.R;
import ir.markazandroid.unimakeup.adapter.MakeupToolChooserAdapter;
import ir.markazandroid.unimakeup.object.MakeupTool;

import static ir.markazandroid.unimakeup.fragment.MakeupMainMenuFragment.MENU_LIPSTICKS;
import static ir.markazandroid.unimakeup.util.Utils.convertARBG2RGBA;

/**
 * Coded by Ali on 9/8/2019.
 */
public class PowderMakeupFragment extends MakeupFragment {

    private MakeupToolChooserAdapter adapter;

    @Override
    protected void init() {
        //switchIV.setImageResource(R.drawable.lipsticks_full);
        switchIV.setOnClickListener(v -> ((MakeupMainMenuFragment.OnMenuSelectListener) getActivity()).onMenuClicked(MENU_LIPSTICKS));
        switchIV2.setClickable(false);

        switchIV.post(() -> {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) switchIV.getLayoutParams();
            params.width /= 2;
            params.height /= 2;
            switchIV.setLayoutParams(params);
        });
        //Was 15%
        mListener.getMakeup().getBilFilter().setCreamOpacity(0.3f);
        mListener.getMakeup().getTouchMask().setFullTouched(true);
        seekBar.setMax(50);
        //Was 15%
        seekBar.setProgress(30);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mListener.getMakeup().getBilFilter().setCreamOpacity((progress / 100f));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        refreshSelected(selectedTool);
    }

    @Override
    protected String getMenuName() {
        return "کرم پودر";
    }

    @Override
    protected void setAdapter(RecyclerView list) {

        adapter = new MakeupToolChooserAdapter(getActivity(), color -> {
            refreshSelected(color);
            //adapter.setSelectedTool(color);
            return false;
        });
/*
        adapter.setColors(Arrays.asList(
                getColorInt(180,55,0),
                getColorInt(224,189,177),getColorInt(214,174,159),
                getColorInt(218,174,142),getColorInt(221,179,147),
                getColorInt(221,165,139),getColorInt(204,158,127)
        ));*/

        adapter.setTools(MakeupTool.getFoundations());

        adapter.setSelectedTool(selectedTool);

        list.setAdapter(adapter);

    }

    @Override
    protected void initClear(View clear) {
        clear.setOnClickListener(v -> refreshSelected(null));
    }

    private void refreshSelected(MakeupTool color) {
        mListener.setFoundation(color);
        selectedTool = color;
        adapter.setSelectedTool(color);

        resetLogos();

        if (color != null) {
            mListener.getMakeup().getBilFilter().setCreamColor(convertARBG2RGBA(color.getColor()));
            mListener.getMakeup().setBilFilterEnabled(true);

            brandIv.setVisibility(View.VISIBLE);
            toolIv.setVisibility(View.VISIBLE);
            brandIv.setImageResource(color.getBrandLogo());
            toolIv.setImageResource(color.getLogo());

            if (MakeupTool.BRAND_BLACK_DIAMOND.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.black_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_black_box);

                largenLogoParams(blackdiamongBrandLogo);

            } else if (MakeupTool.BRAND_CALLISTA.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.pink_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_pink_box);

                largenLogoParams(callistaBrandLogo);

            } else if (MakeupTool.BRAND_SCHON.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.white_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_white_box);

                largenLogoParams(schonBrandLogo);
            } else if (MakeupTool.BRAND_NOTE.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.black_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_black_box);

                largenLogoParams(noteBrandLogo);
            }

            list.smoothScrollToPosition(adapter.getTools().indexOf(color) + 1);
        } else {
            mListener.getMakeup().getBilFilter().setCreamColor(0);
            mListener.getMakeup().setBilFilterEnabled(false);
            brandIv.setVisibility(View.GONE);
            toolIv.setVisibility(View.GONE);
        }
    }

}
