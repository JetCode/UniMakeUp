package ir.markazandroid.unimakeup.fragment;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SeekBar;

import ir.markazandroid.unimakeup.R;
import ir.markazandroid.unimakeup.adapter.MakeupToolChooserAdapter;
import ir.markazandroid.unimakeup.object.MakeupTool;

import static ir.markazandroid.unimakeup.fragment.MakeupMainMenuFragment.MENU_POWDER;
import static ir.markazandroid.unimakeup.util.Utils.convertARBG2RGBA;

/**
 * Coded by Ali on 9/8/2019.
 */
public class LipsticksMakeupFragment extends MakeupFragment {

    private MakeupToolChooserAdapter adapter;

    @Override
    protected void init() {
        switchIV2.setOnClickListener(v -> ((MakeupMainMenuFragment.OnMenuSelectListener) getActivity()).onMenuClicked(MENU_POWDER));
        switchIV.setClickable(false);

        switchIV.post(() -> {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) switchIV2.getLayoutParams();
            params.width /= 2;
            params.height /= 2;
            switchIV2.setLayoutParams(params);
        });

        mListener.getMakeup().getLipsticks().setLipStickOpacity(0.5f);
        seekBar.setMax(99);
        seekBar.setProgress(50);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mListener.getMakeup().getLipsticks().setLipStickOpacity((progress / 100f));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        refreshSelected(selectedTool);
    }

    @Override
    protected String getMenuName() {
        return "رژ لب";
    }

    @Override
    protected void setAdapter(RecyclerView list) {

        adapter = new MakeupToolChooserAdapter(getActivity(), color -> {
            refreshSelected(color);
            //adapter.setSelectedTool(color);
            return false;
        });

        adapter.setTools(MakeupTool.getLipsticks());

        adapter.setSelectedTool(selectedTool);

       /* adapter.setColors(Arrays.asList(
                getColorInt(212,126,107),getColorInt(189,107,111),getColorInt(200,80,91),getColorInt(184,103,125),
                getColorInt(165,88,108),getColorInt(197,57,76),getColorInt(175,39,69),getColorInt(162,81,86),
                getColorInt(207,147,137),getColorInt(168,104,122),getColorInt(153,109,122),getColorInt(133,52,71),
                getColorInt(162,93,92),getColorInt(219,68,86),getColorInt(218,80,64),getColorInt(216,45,63),
                getColorInt(158,49,66),getColorInt(112,43,50),getColorInt(151,59,127),getColorInt(138,52,82)

        ));*/

        list.setAdapter(adapter);

    }

    @Override
    protected void initClear(View clear) {
        clear.setOnClickListener(v -> refreshSelected(null));
    }


    private void refreshSelected(MakeupTool color) {
        mListener.setLipStick(color);
        selectedTool = color;
        adapter.setSelectedTool(color);

        resetLogos();

        if (color != null) {
            mListener.getMakeup().getLipsticks().setLipStickColor(convertARBG2RGBA(color.getColor()));
            mListener.getMakeup().setLipsticksEnabled(true);

            brandIv.setVisibility(View.VISIBLE);
            toolIv.setVisibility(View.VISIBLE);
            brandIv.setImageResource(color.getBrandLogo());
            toolIv.setImageResource(color.getLogo());

            if (MakeupTool.BRAND_BLACK_DIAMOND.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.black_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_black_box);

                largenLogoParams(blackdiamongBrandLogo);

            } else if (MakeupTool.BRAND_CALLISTA.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.pink_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_pink_box);

                largenLogoParams(callistaBrandLogo);

            } else if (MakeupTool.BRAND_SCHON.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.white_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_white_box);

                largenLogoParams(schonBrandLogo);
            } else if (MakeupTool.BRAND_NOTE.equals(color.getBrandName())) {
                brandIv.setBackgroundResource(R.drawable.black_bg);
                toolIv.setBackgroundResource(R.drawable.mat_white_bordered_black_box);

                largenLogoParams(noteBrandLogo);
            }

            list.smoothScrollToPosition(adapter.getTools().indexOf(color) + 1);

        } else {
            mListener.getMakeup().getLipsticks().setLipStickColor(0);
            mListener.getMakeup().setLipsticksEnabled(false);
            brandIv.setVisibility(View.GONE);
            toolIv.setVisibility(View.GONE);
        }
    }

}
