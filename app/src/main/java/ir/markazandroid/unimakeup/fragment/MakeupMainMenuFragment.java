package ir.markazandroid.unimakeup.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ir.markazandroid.unimakeup.R;


public class MakeupMainMenuFragment extends Fragment {


    public static final String MENU_LIPSTICKS = "lipsticks";
    public static final String MENU_POWDER = "powder";


    private OnMenuSelectListener mListener;

    public MakeupMainMenuFragment() {
        // Required empty public constructor
    }


    public static MakeupMainMenuFragment newInstance() {
        MakeupMainMenuFragment fragment = new MakeupMainMenuFragment();
        Bundle args = new Bundle();
        // args.putString(ARG_PARAM1, param1);
        // args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private View lipsticksView, powderView, back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_makeup_main_menu, container, false);
        lipsticksView = view.findViewById(R.id.lipsticks);
        powderView = view.findViewById(R.id.powder);
        back = view.findViewById(R.id.back);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMenuSelectListener) {
            mListener = (OnMenuSelectListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMenuSelectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMenuSelectListener {
        void onMenuClicked(String menu);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lipsticksView.setOnClickListener(v -> mListener.onMenuClicked(MENU_LIPSTICKS));
        powderView.setOnClickListener(v -> mListener.onMenuClicked(MENU_POWDER));
        back.setOnClickListener(v -> getActivity().finish());

    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
