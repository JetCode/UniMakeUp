package ir.markazandroid.unimakeup.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import ir.markazandroid.unimakeup.Makeup;
import ir.markazandroid.unimakeup.R;
import ir.markazandroid.unimakeup.object.MakeupTool;


public abstract class MakeupFragment extends Fragment {


    protected makeupFragmentListener mListener;
    protected boolean finishVisibility;

    public MakeupFragment() {
        // Required empty public constructor
    }


    protected ImageView back;
    protected View menu;
    protected View clear;
    protected SeekBar seekBar;
    protected RecyclerView list;
    protected ImageView switchIV, brandIv, toolIv, finalOk, switchIV2, blackdiamongBrandLogo, callistaBrandLogo, schonBrandLogo, noteBrandLogo;
    protected MakeupTool selectedTool;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.makeup_fragment, container, false);
        back = view.findViewById(R.id.back);
        menu = view.findViewById(R.id.menu);
        list = view.findViewById(R.id.list);
        clear = view.findViewById(R.id.clear);
        seekBar = view.findViewById(R.id.seekBar);
        switchIV = view.findViewById(R.id.switch_iv);
        brandIv = view.findViewById(R.id.brand_iv);
        toolIv = view.findViewById(R.id.tool_iv);
        finalOk = view.findViewById(R.id.final_ok);
        switchIV2 = view.findViewById(R.id.switch_iv2);

        blackdiamongBrandLogo = view.findViewById(R.id.black_diamond_brand_iv);
        callistaBrandLogo = view.findViewById(R.id.callista_brand_iv);
        schonBrandLogo = view.findViewById(R.id.schon_brand_iv);
        noteBrandLogo = view.findViewById(R.id.note_brand_iv);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        back.setOnClickListener(v -> mListener.backToMenu());
        setFinishVisibility(finishVisibility);
        finalOk.setOnClickListener(v -> {
            mListener.end();
        });

        setAdapter(list);
        initClear(clear);
        init();

        TextView menuNameTv = menu.findViewById(R.id.menu_name);
        menuNameTv.setText(getMenuName());

    }

    protected abstract void init();


    protected abstract String getMenuName();

    protected abstract void setAdapter(RecyclerView list);

    protected abstract void initClear(View clear);

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof makeupFragmentListener) {
            mListener = (makeupFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMenuSelectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface makeupFragmentListener {
        Makeup getMakeup();

        void backToMenu();

        void setLipStick(MakeupTool lipstick);

        void setFoundation(MakeupTool foundation);

        void end();
    }

    public void setSelectedTool(MakeupTool selectedTool) {
        this.selectedTool = selectedTool;
    }


    public void setFinishVisibility(boolean visible) {
        if (finalOk != null)
            finalOk.setVisibility(visible ? View.VISIBLE : View.GONE);
        finishVisibility = visible;

    }

    protected void resetLogos() {
        smallenLogoParams(blackdiamongBrandLogo);
        smallenLogoParams(noteBrandLogo);
        smallenLogoParams(callistaBrandLogo);
        smallenLogoParams(schonBrandLogo);
    }

    protected void smallenLogoParams(View view) {
        view.setAlpha(0.7f);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = getContext().getResources().getDimensionPixelSize(R.dimen.brand_logo_width_small);
        params.height = getContext().getResources().getDimensionPixelSize(R.dimen.brand_logo_height_small);
        view.setLayoutParams(params);
    }

    protected void largenLogoParams(View view) {
        view.setAlpha(1f);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = getContext().getResources().getDimensionPixelSize(R.dimen.brand_logo_width_large);
        params.height = getContext().getResources().getDimensionPixelSize(R.dimen.brand_logo_height_large);
        view.setLayoutParams(params);
    }


}
