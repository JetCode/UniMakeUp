package ir.markazandroid.unimakeup;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Coded by Ali on 9/16/2019.
 */
public class SkinMask {

    public static final String BILATERAL_VERTEX_SHADER = "" +
            "attribute vec4 a_position;\n" +
            "attribute vec2 a_texCoord0;\n" +

            "const int GAUSSIAN_SAMPLES = 18;\n" +


            "varying vec2 textureCoordinate;\n" +
            "varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +

            "uniform mat4 u_projTrans;\n" +

            "uniform vec2 singleStepOffset;\n" +

            "void main()\n" +
            "{\n" +
            "	gl_Position = u_projTrans * a_position;\n" +
            "	textureCoordinate = vec2(a_texCoord0.x,1.0-a_texCoord0.y);\n" +

            "	int multiplier = 0;\n" +
            "	vec2 blurStep;\n" +

            "	for (int i = 0; i < GAUSSIAN_SAMPLES/2; i++)\n" +
            "	{\n" +
            "		multiplier = (i - ((GAUSSIAN_SAMPLES/2 - 1) / 2));\n" +

            "		blurStep = float(multiplier) * singleStepOffset;\n" +
            "		blurCoordinates[i].x = textureCoordinate.x + blurStep.x;\n" +
            "		blurCoordinates[i].y = textureCoordinate.y;\n" +
            "	}\n" +
            "	for (int j = 0; j < GAUSSIAN_SAMPLES/2; j++)\n" +
            "	{\n" +
            "		multiplier = (j - ((GAUSSIAN_SAMPLES/2 - 1) / 2));\n" +

            "		blurStep = float(multiplier) * singleStepOffset;\n" +
            "		blurCoordinates[j+(GAUSSIAN_SAMPLES/2)].x = textureCoordinate.x;\n" +
            "		blurCoordinates[j+(GAUSSIAN_SAMPLES/2)].y = textureCoordinate.y + blurStep.y;\n" +
            "	}\n" +
            "}";

    private static final String SKIN_VERTEX_SHADER =
            ""
                    + "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
                    + "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
                    + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
                    //+ "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "1;\n" //
                    + "uniform mat4 u_projTrans;\n" //
                    + "varying vec4 v_color;\n" //
                    + "varying vec2 v_texCoords;\n" //
                    //+ "varying vec2 v_texCoords1;\n" //
                    + "\n" //
                    + "void main()\n" //
                    + "{\n" //
                    + "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
                    // + "   v_color.a = v_color.a * (255.0/254.0);\n" //
                    + "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
                    // + "   v_texCoords1 = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "1;\n" //
                    + "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
                    + "}\n";

    private static final String SKIN_FRAGMENT_SHADER =
            ""
                    + "#ifdef GL_ES\n" //
                    + "#define LOWP lowp\n" //
                    + "precision mediump float;\n" //
                    + "#else\n" //
                    + "#define LOWP \n" //
                    + "#endif\n" //
                    + "varying LOWP vec4 v_color;\n" //
                    + "varying vec2 v_texCoords;\n" //
                    // + "varying vec2 v_texCoords1;\n" //
                    + "uniform sampler2D u_texture;\n" //
                    + "uniform sampler2D u_texture1;\n"
                    //  + "uniform float lipStickOpacity;\n" //
                    + "void main()\n"//
                    + "{\n" //
                    + "  vec4 sum1 = texture2D(u_texture, v_texCoords);\n" +
                    // + "  vec2 cords = v_texCoords;\n"
                    // + "  cords.y = 1.0 - cords.y;\n"

                    "     float mask = texture2D(u_texture1, vec2(textureCoordinate.x,1.0-textureCoordinate.y)).r;" +

                    "     vec4 thr = vec4(70.0/255.0, 125.0/255.0, 130.0/255.0, 180.0/255.0);" +

                    "     float Y = (0.257 * sum1.r) + (0.504 * sum1.g) + (0.098 * sum1.b) + 16.0/256.0;\n" +
                    "     float V = (0.439 *  sum1.r) - (0.368 *  sum1.g) - (0.071 * sum1.b) + 128.0/256.0;\n" +
                    "     float U = -(0.148 *  sum1.r) - (0.291 *  sum1.g) + (0.439 * sum1.b) + 128.0/256.0;\n" +

                    "     float uPL = smoothstep(thr[0],thr[1],U);\n" +
                    "     float uP = ((1.0-uPL) * uPL )* 4.0;\n" +

                    "     float vPL = smoothstep(thr[2],thr[3],V);\n" +
                    "     float vP = ((1.0-vPL) * vPL )* 4.0;\n" +

                    "     float isSkin = ((uP + vP) /2.0) * step(thr[0], Y) ;\n"


                    // +"     float Y = (0.257 * texColor.r) + (0.504 * texColor.g) + (0.098 * texColor.b) + 16.0/256.0;\n"

                    //  "   vec4 spoted = mix(sum1,spotColor,isSkin * spotColor.a);                              \n"

                    //     + "  float a = lipSticksTexColor.a;"
                    //     + "  texColor.rgb = mix(texColor.rgb, lipSticksTexColor.rgb ,a * lipStickOpacity * Y );\n"
                    //     + "  gl_FragColor =mask.r*0.000001+vec4(vec3(isSkin > 0),1.0) ;\n" //
                    + "  gl_FragColor = mask.r == 1.0 ? vec4(isSkin):vec4(0.0) ;\n" //
                    + "}";


    public static final String BILATERAL_FRAGMENT_SHADER = "" +
            "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" +

            "uniform sampler2D u_texture;\n" +

            "uniform sampler2D u_texture1;\n" +


            " const int GAUSSIAN_SAMPLES = 18;\n" +

            " varying vec2 textureCoordinate;\n" +
            " varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +

            " uniform float distanceNormalizationFactor;\n" +

            " void main()\n" +
            " {\n" +
            "       vec4 centralColor;\n" +
            "       float gaussianWeightTotal;\n" +
            "      vec4 sum;\n" +
            "       vec4 sampleColor;\n" +
            "       float distanceFromCentralColor;\n" +
            "      float gaussianWeight;\n" +
            "     \n" +
            "     centralColor = texture2D(u_texture, blurCoordinates[4]);\n" +
            "     gaussianWeightTotal = 0.2270270270;\n" +
            "     sum = centralColor * 0.2270270270;\n" +
            "     \n" +
            // X
            "     sampleColor = texture2D(u_texture, blurCoordinates[0]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[1]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[2]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[3]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[5]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[6]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[7]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[8]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +
            //Y
            "     sampleColor = texture2D(u_texture, blurCoordinates[9]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[10]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[11]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[12]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[14]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[15]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[16]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[17]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     vec4 sum1 = sum / gaussianWeightTotal;" +

            "      vec4 thr = vec4(70.0/255.0, 125.0/255.0, 130.0/255.0, 180.0/255.0);" +

            "     float Y = (0.257 * sum1.r) + (0.504 * sum1.g) + (0.098 * sum1.b) + 16.0/256.0;\n" +
            "     float V = (0.439 *  sum1.r) - (0.368 *  sum1.g) - (0.071 * sum1.b) + 128.0/256.0;\n" +
            "     float U = -(0.148 *  sum1.r) - (0.291 *  sum1.g) + (0.439 * sum1.b) + 128.0/256.0;\n" +

            "     float uPL = smoothstep(thr[0],thr[1],U);\n" +
            "     float uP = ((1.0-uPL) * uPL )* 4.0;\n" +

            "     float vPL = smoothstep(thr[2],thr[3],V);\n" +
            "     float vP = ((1.0-vPL) * vPL )* 4.0;\n" +

            "     float isSkin = ((uP + vP) /2.0) * step(thr[0], Y) ;\n" +

            "     float mask = texture2D(u_texture1, vec2(textureCoordinate.x,1.0-textureCoordinate.y)).r;" +

            //"     Y = Y*1.2;\n"+

            // "     U = U*1.2;\n"+

            //  "     V = V*1.2;\n"+


            //"     float vP = smoothstep(thr[2],thr[3],V);\n"+


            // "     float vP = smoothstep(thr[2],thr[3],V);\n"+


            //  "      Y =  (Y > 73.0/255.0) &&  \n" +
            //    "      (U >= thr[0]) && (U <= thr[1]) && \n" +
            //    "      (V >= thr[2]) && (V <=thr[3]) ? 1.0 : 0.0; \n" +

            // "   float r, g, b, a;                         \n" +

            // " Y = 1.1643 * ( Y - 0.0625 );\n" +

            // "\n" +
            // "            U = U - 0.5;\n" +
            // "            V = V - 0.5;\n" +
            // "\n" +
            // "            r = Y + 1.5958 * V;\n" +
            // "            g = Y - 0.39173 * U - 0.81290 * V;\n" +
            // "            b = Y + 2.017 * U;\n" +
            // "\n" +
            //     "            sum1 = vec4(r,g,b, 1.0);"

            // "   sum1 = mix(sum1,vec4(180.0/255.0,55.0/255.0,0.0/255.0,1.0),isSkin*creamOpacity);                              \n" +


            //   "      sum1 = vec4(vec3(( (Y > thr[0]) && \n" +
            //  "      (U >= thr[0]) && (U <= thr[1]) && \n" +
            //  "      (V >= thr[2]) && (V <=thr[3])) ? 1.0 : 0.0), \n" +
            //  "      1.0);"+
            //  "     sum1 = mix(sum1,sum1* vec4(0.9,0.9,0.9,1)=,0.6);"+
            //        "     vec4 maskColor = texture2D(mask, textureCoordinate);"+

            //     "     gl_FragColor =vec4(vec3(isSkin),1.0) + sum1 * 0.000001 ;\n" +

            //    "     gl_FragColor = vec4(sum1.rgb,isTouched * isSkin);\n" +


            "  gl_FragColor = mask == 1.0 ? vec4(vec3(isSkin),1.0):vec4(0.0) ;\n" +
            " }";

    public static final String BILATERAL_FRAGMENT_SHADER_BLUR = "" +
            "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" +

            "uniform sampler2D u_texture;\n" +

            " const int GAUSSIAN_SAMPLES = 18;\n" +

            " varying vec2 textureCoordinate;\n" +
            " varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +

            " uniform float distanceNormalizationFactor;\n" +

            " void main()\n" +
            " {\n" +
            "       vec4 centralColor;\n" +
            "       float gaussianWeightTotal;\n" +
            "      vec4 sum;\n" +
            "       vec4 sampleColor;\n" +
            "       float distanceFromCentralColor;\n" +
            "      float gaussianWeight;\n" +
            "     \n" +
            "     centralColor = texture2D(u_texture, blurCoordinates[4]);\n" +
            "     gaussianWeightTotal = 0.2270270270;\n" +
            "     sum = centralColor * 0.2270270270;\n" +
            "     \n" +
            // X
            "     sampleColor = texture2D(u_texture, blurCoordinates[0]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[1]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[2]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[3]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[5]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[6]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[7]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[8]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +
            //Y
            "     sampleColor = texture2D(u_texture, blurCoordinates[9]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[10]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[11]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[12]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[14]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1945945946 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[15]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.1216216216 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[16]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0540540541 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     sampleColor = texture2D(u_texture, blurCoordinates[17]);\n" +
            "     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);\n" +
            "     gaussianWeight = 0.0162162162 * (1.0 - distanceFromCentralColor);\n" +
            "     gaussianWeightTotal += gaussianWeight;\n" +
            "     sum += sampleColor * gaussianWeight;\n" +

            "     vec4 sum1 = sum / gaussianWeightTotal;" +


            //"     Y = Y*1.2;\n"+

            // "     U = U*1.2;\n"+

            //  "     V = V*1.2;\n"+


            //"     float vP = smoothstep(thr[2],thr[3],V);\n"+


            // "     float vP = smoothstep(thr[2],thr[3],V);\n"+


            //  "      Y =  (Y > 73.0/255.0) &&  \n" +
            //    "      (U >= thr[0]) && (U <= thr[1]) && \n" +
            //    "      (V >= thr[2]) && (V <=thr[3]) ? 1.0 : 0.0; \n" +

            // "   float r, g, b, a;                         \n" +

            // " Y = 1.1643 * ( Y - 0.0625 );\n" +

            // "\n" +
            // "            U = U - 0.5;\n" +
            // "            V = V - 0.5;\n" +
            // "\n" +
            // "            r = Y + 1.5958 * V;\n" +
            // "            g = Y - 0.39173 * U - 0.81290 * V;\n" +
            // "            b = Y + 2.017 * U;\n" +
            // "\n" +
            //     "            sum1 = vec4(r,g,b, 1.0);"

            // "   sum1 = mix(sum1,vec4(180.0/255.0,55.0/255.0,0.0/255.0,1.0),isSkin*creamOpacity);                              \n" +


            //   "      sum1 = vec4(vec3(( (Y > thr[0]) && \n" +
            //  "      (U >= thr[0]) && (U <= thr[1]) && \n" +
            //  "      (V >= thr[2]) && (V <=thr[3])) ? 1.0 : 0.0), \n" +
            //  "      1.0);"+
            //  "     sum1 = mix(sum1,sum1* vec4(0.9,0.9,0.9,1)=,0.6);"+
            //        "     vec4 maskColor = texture2D(mask, textureCoordinate);"+

            //     "     gl_FragColor =vec4(vec3(isSkin),1.0) + sum1 * 0.000001 ;\n" +

            //    "     gl_FragColor = vec4(sum1.rgb,isTouched * isSkin);\n" +


            "  gl_FragColor = vec4(sum1.rgb,1.0);\n" +
            " }";


    private FrameBuffer skinMaskFrameBuffer;
    private ShaderProgram skinDetectorShaderProgram;
    private TextureRegion region;
    private ShaderProgram blurShaderProgram;
    private FrameBuffer finalSkinBuffer;
    private TextureRegion region2;

    public void init() {

        skinMaskFrameBuffer = new FrameBuffer(Pixmap.Format.RGB565, Makeup.SCREEN_WIDTH, Makeup.SCREEN_HEIGHT, false);
        finalSkinBuffer = new FrameBuffer(Pixmap.Format.RGB565, Makeup.SCREEN_WIDTH, Makeup.SCREEN_HEIGHT, false);

        skinDetectorShaderProgram = new ShaderProgram(BILATERAL_VERTEX_SHADER, BILATERAL_FRAGMENT_SHADER);
        if (skinDetectorShaderProgram.getLog().length() != 0)
            Log.e("Shader Compiling", skinDetectorShaderProgram.getLog());

        skinDetectorShaderProgram.begin();
        skinDetectorShaderProgram.setUniformi("u_texture1", 1);
        skinDetectorShaderProgram.setUniformf("singleStepOffset", 12.0f / 1080, 12.0f / 1920);
        skinDetectorShaderProgram.setUniformf("distanceNormalizationFactor", 4f);
        skinDetectorShaderProgram.end();

        blurShaderProgram = new ShaderProgram(BilFilter.BILATERAL_VERTEX_SHADER, BILATERAL_FRAGMENT_SHADER_BLUR);
        if (blurShaderProgram.getLog().length() != 0)
            Log.e("BShader Compiling", blurShaderProgram.getLog());

        blurShaderProgram.begin();
        blurShaderProgram.setUniformf("singleStepOffset", 5.0f / 1080, 5.0f / 1920);
        blurShaderProgram.setUniformf("distanceNormalizationFactor", 1f);
        blurShaderProgram.end();

        region = new TextureRegion(skinMaskFrameBuffer.getColorBufferTexture());
        //region.flip(false,true);

        region2 = new TextureRegion(finalSkinBuffer.getColorBufferTexture());
        //region2.flip(false,true);

    }

    public void draw(Batch batch, TextureRegion cameraData, TextureRegion mask) {
        ShaderProgram s = batch.getShader();

        batch.setShader(skinDetectorShaderProgram);

        skinMaskFrameBuffer.begin();
        Gdx.gl.glClearColor(0f, 0f, 0f, 0f); //transparent black
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE1);
        mask.getTexture().bind();

        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
        cameraData.getTexture().bind();

        batch.draw(cameraData, 0, 0);

        batch.flush();

        skinMaskFrameBuffer.end();

 /*       finalSkinBuffer.begin();

        batch.setShader(blurShaderProgram);

        batch.draw(region.getTexture(),0,0);

        batch.flush();
        finalSkinBuffer.end();
*/
        batch.setShader(s);
    }


    public TextureRegion getMaskTexture() {
        return region;
    }

    public void dispose() {
        skinMaskFrameBuffer.dispose();
        skinDetectorShaderProgram.dispose();
        blurShaderProgram.dispose();
        finalSkinBuffer.dispose();
    }
}
