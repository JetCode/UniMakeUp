package ir.markazandroid.unimakeup;

import android.graphics.PointF;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;

/**
 * Coded by Ali on 9/15/2019.
 */
public class FaceMask {

    private final static String vertexShader = "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
            + "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
            + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
            + "uniform mat4 u_projTrans;\n" //
            + "varying vec4 v_color;\n" //
            + "varying vec2 v_texCoords;\n" //
            + "\n" //
            + "void main()\n" //
            + "{\n" //
            + "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
            + "   v_color.a = v_color.a * (255.0/254.0);\n" //
            //  + "   a_texCoord0.y = 1.0 - a_texCoord0.y;\n" //
            + "   v_texCoords.x = a_texCoord0.x;\n" //
            + "   v_texCoords.y = a_texCoord0.y;\n" //
            + "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
            + "}\n";
    private final static String fragmentShader = "#ifdef GL_ES\n" //
            + "#define LOWP lowp\n" //
            + "precision mediump float;\n" //
            + "#else\n" //
            + "#define LOWP \n" //
            + "#endif\n" //
            + "varying LOWP vec4 v_color;\n" //
            + "varying vec2 v_texCoords;\n" //
            + "uniform sampler2D u_texture;\n" //
            + "void main()\n"//
            + "{\n" //
            + "  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n" //
            + "}";

    private FrameBuffer maskFrameBuffer;
    private Texture textureSolidBlack, textureSolidWhite;
    private PolygonSprite poly;
    private EarClippingTriangulator earClippingTriangulator;
    private CatmullRomSpline<Vector2> crs;
    private ShaderProgram simpleShaderProgram;
    private TextureRegion textureRegion;

    public void init() {

        Pixmap pix = new Pixmap(1, 1, Pixmap.Format.RGB565);
        pix.setColor(0.0f, 0f, 0f, 0f);
        pix.fill();
        textureSolidBlack = new Texture(pix);

        pix = new Pixmap(1, 1, Pixmap.Format.RGB565);
        pix.setColor(1f, 1f, 1f, 1f);
        pix.fill();
        textureSolidWhite = new Texture(pix);

        earClippingTriangulator = new EarClippingTriangulator();

        simpleShaderProgram = new ShaderProgram(vertexShader, fragmentShader);

        maskFrameBuffer = new FrameBuffer(Pixmap.Format.RGB565, Makeup.SCREEN_WIDTH, Makeup.SCREEN_HEIGHT, false);

        textureRegion = new TextureRegion(maskFrameBuffer.getColorBufferTexture());
        //textureRegion.flip(false,true);

    }

    private ShaderProgram defaultShaderProgram;

    public void beginEditingMask(Batch batch) {
        defaultShaderProgram = batch.getShader();

        batch.setShader(simpleShaderProgram);
        maskFrameBuffer.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT
                | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

    }

    public void maskInclude(PolygonSpriteBatch batch, PointF[] points) {
        inf(batch, points, textureSolidWhite);
    }

    public void maskExclude(PolygonSpriteBatch batch, PointF[] points) {
        inf(batch, points, textureSolidBlack);
    }

    public void endEditingMask(Batch batch) {
        batch.flush();
        maskFrameBuffer.end();

        batch.setShader(defaultShaderProgram);
    }


    public TextureRegion getMaskTexture() {
        return textureRegion;
    }

    private void inf(PolygonSpriteBatch batch, PointF[] points, Texture texture) {

        Vector2[] vectorPoints = new Vector2[points.length];
        for (int i = 0; i < points.length; i++) {
            vectorPoints[i] = new Vector2(points[i].x, points[i].y);
        }
        crs = new CatmullRomSpline<>(vectorPoints, false);

        float[] pts = new float[506];
        Vector2 temp = new Vector2();
        pts[0] = vectorPoints[0].x;
        pts[1] = vectorPoints[0].y;
        for (int i = 0; i < pts.length / 2 - 2; i++) {
            crs.valueAt(temp, i / (float) (pts.length / 2 - 3));
            pts[(i + 1) * 2] = temp.x;
            pts[(i + 1) * 2 + 1] = temp.y;
        }
        pts[pts.length - 2] = vectorPoints[vectorPoints.length - 1].x;
        pts[pts.length - 1] = vectorPoints[vectorPoints.length - 1].y;
// Creating the color filling (but textures would work the same way)


        PolygonRegion polyReg = new PolygonRegion(new TextureRegion(texture),
                pts, earClippingTriangulator.computeTriangles(pts).items);
        poly = new PolygonSprite(polyReg);


        //poly.setOrigin(a, b);
        //polyBatch.begin();
        //Gdx.gl.glColorMask(false, false, false, true);

        //change the blending function for our alpha map
        //polyBatch.setBlendFunction(GL20.GL_ONE_MINUS_DST_COLOR, GL20.GLL);

        //TODO
        poly.draw(batch);
        //polyBatch.end();
    }

    public void dispose() {
        maskFrameBuffer.dispose();
        textureSolidBlack.dispose();
        textureSolidWhite.dispose();
        simpleShaderProgram.dispose();
    }
}
