package ir.markazandroid.unimakeup;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ir.markazandroid.unimakeup.object.MakeupTool;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FinalActivity extends AppCompatActivity {


    public static final String EXTRA_LIPSTICK = "ir.markazandroid.unimakeup.FinalActivity.EXTRA_LIPSTICK";
    public static final String EXTRA_FOUNDATION = "ir.markazandroid.unimakeup.FinalActivity.EXTRA_FOUNDATION";

    private MakeupTool lipsticks, foundation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        lipsticks = (MakeupTool) getIntent().getSerializableExtra(EXTRA_LIPSTICK);
        foundation = (MakeupTool) getIntent().getSerializableExtra(EXTRA_FOUNDATION);

        if (lipsticks == null && foundation != null) {
            setupTopTool(foundation);
        } else if (lipsticks != null && foundation == null) {
            setupTopTool(lipsticks);
        } else if (foundation != null) {
            setupTopTool(foundation);
            setupBotTool(lipsticks);
        }


        findViewById(R.id.back).setOnClickListener(v -> {
            Intent intent = new Intent(FinalActivity.this, StarterActivity.class);
            startActivity(intent);
            finish();
        });

    }

    private void setupTopTool(MakeupTool tool) {
        ((ImageView) findViewById(R.id.lip_color_iv)).setImageDrawable(new ColorDrawable(tool.getColor()));
        ((ImageView) findViewById(R.id.lip_tool_iv)).setImageResource(tool.getLogo());
        ((TextView) findViewById(R.id.lip_name_tv)).setText(tool.getName());
        ((TextView) findViewById(R.id.lip_brand_tv)).setText(tool.getBrandName());
        ((TextView) findViewById(R.id.lip_label)).setText(tool.getCode());
    }

    private void setupBotTool(MakeupTool tool) {
        findViewById(R.id.powder_color_iv).setVisibility(View.VISIBLE);
        findViewById(R.id.powder_tool_iv).setVisibility(View.VISIBLE);
        findViewById(R.id.powder_name_tv).setVisibility(View.VISIBLE);
        findViewById(R.id.powder_brand_tv).setVisibility(View.VISIBLE);
        findViewById(R.id.powder_label).setVisibility(View.VISIBLE);

        findViewById(R.id.sep1).setVisibility(View.VISIBLE);
        findViewById(R.id.sep2).setVisibility(View.VISIBLE);
        findViewById(R.id.sep3).setVisibility(View.VISIBLE);
        findViewById(R.id.sep4).setVisibility(View.VISIBLE);
        findViewById(R.id.sep5).setVisibility(View.VISIBLE);

        ((ImageView) findViewById(R.id.powder_color_iv)).setImageDrawable(new ColorDrawable(tool.getColor()));
        ((ImageView) findViewById(R.id.powder_tool_iv)).setImageResource(tool.getLogo());
        ((TextView) findViewById(R.id.powder_name_tv)).setText(tool.getName());
        ((TextView) findViewById(R.id.powder_brand_tv)).setText(tool.getBrandName());
        ((TextView) findViewById(R.id.powder_label)).setText(tool.getCode());
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
