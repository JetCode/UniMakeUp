package ir.markazandroid.unimakeup;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.util.Objects;

import ir.markazandroid.unimakeup.fragment.LipsticksMakeupFragment;
import ir.markazandroid.unimakeup.fragment.MakeupFragment;
import ir.markazandroid.unimakeup.fragment.MakeupMainMenuFragment;
import ir.markazandroid.unimakeup.fragment.PowderMakeupFragment;
import ir.markazandroid.unimakeup.fragment.TitleFragment;
import ir.markazandroid.unimakeup.object.MakeupTool;
import ir.markazandroid.unimakeup.signal.Signal;
import ir.markazandroid.unimakeup.signal.SignalManager;
import ir.markazandroid.unimakeup.signal.SignalReceiver;
import ir.markazandroid.unimakeup.util.Utils;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CameraViewActivity extends AndroidApplication implements MakeupFragment.makeupFragmentListener, MakeupMainMenuFragment.OnMenuSelectListener, SignalReceiver {


    public static final String ACTION_MAKEUP_CAMERA = "ir.markazandroid.unimakeup.CameraViewActivity.ACTION_MAKEUP_CAMERA";
    public static final String ACTION_SCENARIO = "ir.markazandroid.unimakeup.CameraViewActivity.ACTION_SCENARIO";

    private Makeup makeup;

    private MakeupTool selectedLipsticks, selectedFoundation;
    private MakeupFragment currentMakeupFragment;

    private View mainView;
    private View makeupCameraButtonPb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSignalManager().addReceiver(this);

        setContentView(R.layout.main_activity);
        makeupCameraButtonPb = findViewById(R.id.makeupCameraButtonPb);
        // View root =getWindow().getDecorView();
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.a = 8;
        cfg.b = 8;
        cfg.g = 8;
        cfg.r = 8;
        cfg.numSamples = 2;

        makeup = new Makeup(this);
        mainView = initializeForView(new CameraRenderer(makeup, this), cfg);
        ((FrameLayout) findViewById(R.id.cameraView)).addView(mainView);
        findViewById(R.id.cameraView).setVisibility(View.INVISIBLE);
        //mainView.setVisibility(View.INVISIBLE);
        //initialize(new CameraRenderer(cameraControl), cfg);
        //graphics.setContinuousRendering(false);
        graphics.getView().setKeepScreenOn(true);


        switch (Objects.requireNonNull(getIntent().getAction())) {
            case ACTION_MAKEUP_CAMERA:
                setupMakeupCamera();
                break;

            case ACTION_SCENARIO:
                setupScenario();
                break;

        }








/*
        SeekBar seekBar = findViewById(R.id.seekBar);
        seekBar.setProgress(15);

        seekBar.setMax(99);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                cameraControl.setCreamOpacity(progress/100f);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        SeekBar seekBar2 = findViewById(R.id.seekBar2);
        seekBar2.setProgress(50);

        seekBar2.setMax(99);
        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                cameraControl.setLipStickOpacity(progress/100f);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ImageButton finisher = findViewById(R.id.finisher);
        finisher.setOnClickListener(v -> onBackPressed());

        RecyclerView list = findViewById(R.id.colorChooser);
        list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,true));
        ColorChooserAdapter adapter = new ColorChooserAdapter(this, color -> {
            cameraControl.setLipStickColor(convertARBG2RGBA(color));
            return false;
        });
        list.setAdapter(adapter);
        adapter.setColors(Arrays.asList(
                getColorInt(212,126,107),getColorInt(189,107,111),getColorInt(200,80,91),getColorInt(184,103,125),
                getColorInt(165,88,108),getColorInt(197,57,76),getColorInt(175,39,69),getColorInt(162,81,86),
                getColorInt(207,147,137),getColorInt(168,104,122),getColorInt(153,109,122),getColorInt(133,52,71),
                getColorInt(162,93,92),getColorInt(219,68,86),getColorInt(218,80,64),getColorInt(216,45,63),
                getColorInt(158,49,66),getColorInt(112,43,50),getColorInt(151,59,127),getColorInt(138,52,82)

                ));
        adapter.setSelectedColor(getColorInt(212,126,107));
        cameraControl.setLipStickColor(convertARBG2RGBA(adapter.getSelectedColor()));
*/

    }


    private void setupScenario() {
        TitleFragment fragment = new TitleFragment();
        fragment.setTitleText("سناریو");
        getFragmentManager().beginTransaction().add(R.id.container, fragment).commit();

        makeup.setSpotsEnabled(true);
        makeup.setBilFilterEnabled(true);
        makeup.setTouchMaskEnabled(true);
    }

    private void setupMakeupCamera() {
        currentMakeupFragment = null;
        Handler handler = new Handler(getMainLooper());
        //handler.post(() -> onMenuClicked(MakeupMainMenuFragment.MENU_POWDER));
        // getFragmentManager().beginTransaction().add(R.id.container, MakeupMainMenuFragment.newInstance()).commit();
        //graphics.getView().post(() -> onMenuClicked(MakeupMainMenuFragment.MENU_POWDER));

    }


    @Override
    public void onMenuClicked(String menu) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out);

        MakeupFragment fragment = null;
        switch (menu) {
            case MakeupMainMenuFragment.MENU_LIPSTICKS:
                fragment = new LipsticksMakeupFragment();
                fragment.setSelectedTool(selectedLipsticks);
                break;

            case MakeupMainMenuFragment.MENU_POWDER:
                fragment = new PowderMakeupFragment();
                fragment.setSelectedTool(selectedFoundation);
                break;
        }

        currentMakeupFragment = fragment;
        refreshFinishButton();
        transaction.replace(R.id.container, fragment);
        transaction.commit();

    }

    @Override
    public Makeup getMakeup() {
        return makeup;
    }

    @Override
    public void backToMenu() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.replace(R.id.container, new MakeupMainMenuFragment()).commit();
        currentMakeupFragment = null;
    }

    @Override
    public void setLipStick(MakeupTool lipstick) {
        selectedLipsticks = lipstick;
        refreshFinishButton();
    }

    @Override
    public void setFoundation(MakeupTool foundation) {
        selectedFoundation = foundation;
        refreshFinishButton();
    }

    @Override
    public void end() {
        Intent intent = new Intent(this, FinalActivity.class);
        intent.putExtra(FinalActivity.EXTRA_FOUNDATION, selectedFoundation);
        intent.putExtra(FinalActivity.EXTRA_LIPSTICK, selectedLipsticks);
        startActivity(intent);
        finish();
    }

    private void refreshFinishButton() {
        if (currentMakeupFragment != null) {
            currentMakeupFragment.setFinishVisibility(selectedFoundation != null || selectedLipsticks != null);
        }
    }

    private SignalManager getSignalManager() {
        return ((UniMakeUpApplication) getApplicationContext()).getSignalManager();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSignal(Signal signal) {
        if (signal.getType() == Signal.SIGNAL_MAKE_UP_CREATED) {
            Log.e("Activity", "Signal Received");
            onMenuClicked(MakeupMainMenuFragment.MENU_POWDER);
            findViewById(R.id.cameraView).setVisibility(View.VISIBLE);
            getSignalManager().sendMainSignal(new Signal(Signal.SIGNAL_VIEW_IS_SHOWN));
            runOnUiThread(() -> {
                        Gdx.graphics.setContinuousRendering(true);
                        Utils.fadeFade(makeupCameraButtonPb, 500, true);
                        //makeupCameraButtonPb.setVisibility(View.GONE);
                    }
            );
            //;
        }

        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSignalManager().removeReceiver(this);
    }
}
