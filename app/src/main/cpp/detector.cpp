//
// Created by Ali on 5/20/2019.
//

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
#include <dlib/image_io.h>
#include <dlib/opencv.h>
#include <dlib/opencv/to_open_cv.h>
#include <dlib/opencv/cv_image.h>
#include <opencv2/core/mat.hpp>
#include <android/log.h>
#include <jni.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <utility>
#include "profiler.h"
#include "yuv2rgb.h"
#include "stdio.h"
#include "stdlib.h"


#define LOGI(...) \
  ((void)__android_log_print(ANDROID_LOG_INFO, "dlib-jni:", __VA_ARGS__))

#define JNI_METHOD(NAME) \
    Java_ir_markazandroid_unimakeup_facedetect_FaceDetector_##NAME


void convertMatAddressToArray2d(JNIEnv *env,
                                long addrInputImage,
                                dlib::array2d<dlib::bgr_pixel> &out) {

    auto *pInputImage = (cv::Mat *) addrInputImage;

    //void* pixels;
    //int state;

    //LOGI("L%d: info.width=%d, info.height=%d", __LINE__, bitmapInfo.width, bitmapInfo.height);
    //out.set_size((long) pInputImage->rows, pInputImage->cols);

    //dlib::array2d<dlib::bgr_pixel> dlibImage;
    dlib::assign_image(out, dlib::cv_image<dlib::bgr_pixel>(*pInputImage));

    /* char* line = (char*) pixels;
     for (int h = 0; h < pInputImage->rows; ++h) {
         for (int w = 0; w < pInputImage->cols; ++w) {
             uint32_t* color = (uint32_t*) (line + 4 * w);

             out[h][w].red = (unsigned char) (0xFF & ((*color) >> 24));
             out[h][w].green = (unsigned char) (0xFF & ((*color) >> 16));
             out[h][w].blue = (unsigned char) (0xFF & ((*color) >> 8));
         }

         line = line + bitmapInfo.stride;
     }

     // Unlock the bitmap.
     AndroidBitmap_unlockPixels(env, bitmap);*/
}

using namespace cv;
Mat outMat;

void convertGrayMatAddressToArray2d(JNIEnv *env,
                                    long addrInputImage,
                                    dlib::array2d<unsigned char> &out) {

    auto *pInputImage = (cv::Mat *) addrInputImage;

    //void* pixels;
    //int state;

    //LOGI("L%d: info.width=%d, info.height=%d", __LINE__, bitmapInfo.width, bitmapInfo.height);
    //out.set_size((long) pInputImage->rows, pInputImage->cols);

    //dlib::array2d<dlib::bgr_pixel> dlibImage;

    /* char* line = (char*) pixels;
     for (int h = 0; h < pInputImage->rows; ++h) {
         for (int w = 0; w < pInputImage->cols; ++w) {
             uint32_t* color = (uint32_t*) (line + 4 * w);

             out[h][w].red = (unsigned char) (0xFF & ((*color) >> 24));
             out[h][w].green = (unsigned char) (0xFF & ((*color) >> 16));
             out[h][w].blue = (unsigned char) (0xFF & ((*color) >> 8));
         }

         line = line + bitmapInfo.stride;
     }

     // Unlock the bitmap.
     AndroidBitmap_unlockPixels(env, bitmap);*/

    dlib::assign_image(out, dlib::cv_image<unsigned char>(*pInputImage));
}

dlib::shape_predictor sFaceLandmarksDetector;
dlib::frontal_face_detector sFaceDetector;

extern "C" JNIEXPORT jboolean JNICALL
JNI_METHOD(isFaceDetectorReady)(JNIEnv *env,
                                jobject thiz) {
    if (sFaceDetector.num_detectors() > 0) {
        return JNI_TRUE;
    } else {
        return JNI_FALSE;
    }
}

extern "C" JNIEXPORT jboolean JNICALL
JNI_METHOD(isFaceLandmarksDetectorReady)(JNIEnv *env,
                                         jobject thiz) {
    if (sFaceLandmarksDetector.num_parts() > 0) {
        return JNI_TRUE;
    } else {
        return JNI_FALSE;
    }
}

extern "C" JNIEXPORT void JNICALL
JNI_METHOD(prepareFaceDetector)(JNIEnv *env,
                                jobject thiz) {

    // Prepare the detector.
    sFaceDetector = dlib::get_frontal_face_detector();

    // double interval = profiler.stopAndGetInterval();

    //LOGI("L%d: sFaceDetector is initialized (took %.3f ms)", __LINE__, interval);
    //LOGI("L%d: sFaceDetector.num_detectors()=%lu", __LINE__, sFaceDetector.num_detectors());
}

extern "C" JNIEXPORT void JNICALL
JNI_METHOD(prepareFaceLandmarksDetector)(JNIEnv *env,
                                         jobject thiz,
                                         jstring detectorPath) {
    const char *path = env->GetStringUTFChars(detectorPath, JNI_FALSE);

    // Profiler.
    //Profiler profiler;
    //profiler.start();

    // We need a shape_predictor. This is the tool that will predict face
    // landmark positions given an image and face bounding box.  Here we are just
    // loading the model from the shape_predictor_68_face_landmarks.dat file you gave
    // as a command line argument.
    // Deserialize the shape detector.
    dlib::deserialize(path) >> sFaceLandmarksDetector;

    ///double interval = profiler.stopAndGetInterval();

    //LOGI("L%d: sFaceLandmarksDetector is initialized (took %.3f ms)", __LINE__, interval);
    //LOGI("L%d: sFaceLandmarksDetector.num_parts()=%lu", __LINE__, sFaceLandmarksDetector.num_parts());

    env->ReleaseStringUTFChars(detectorPath, path);

    if (sFaceLandmarksDetector.num_parts() != 68) {
        //throwException(env, "It's not a 68 landmarks detector!");
    }
}




using namespace cv;

extern "C" JNIEXPORT void JNICALL
JNI_METHOD(detectLandmark)(JNIEnv *env,
                           jobject thiz,
                           jlong matAddress,
                           jlong pointAddress) {
    if (sFaceDetector.num_detectors() == 0) {
        LOGI("L%d: sFaceDetector is not initialized!", __LINE__);
        //throwException(env, "sFaceDetector is not initialized!");
        // return NULL;
        return;
    }
    if (sFaceLandmarksDetector.num_parts() == 0) {
        LOGI("L%d: sFaceLandmarksDetector is not initialized!", __LINE__);
        //throwException(env, "sFaceLandmarksDetector is not initialized!");
        // return NULL;
        return;
    }

    // Profiler.
    Profiler profiler;
    profiler.start();
    // Convert bitmap to dlib::array2d.
    //dlib::array2d<dlib::bgr_pixel> img;
    //dlib::array2d<unsigned char> img;
    //convertGrayMatAddressToArray2d(env, matAddress, img);





//    // Make the image larger so we can detect small faces.
    //dlib::pyramid_up(img);
    //LOGI("L%d: pyramid_up the input image (w=%lu, h=%lu).", __LINE__, img.nc(), img.nr());

    auto *im = (cv::Mat *) matAddress;
    cv::Mat im_small, im_display;

    int resize = 4;

// Resize image for face detection
    cv::resize(*im, im_small, cv::Size(), 1.0 / resize, 1.0 / resize);




// Change to dlib's image format. No memory is copied.
    dlib::cv_image<unsigned char> cimg_small(im_small);
    dlib::cv_image<unsigned char> cimg(*im);


    const float width = (float) cimg_small.nc();
    const float height = (float) cimg_small.nr();
    double interval = profiler.stopAndGetInterval();
    LOGI("L%d: input image (w=%f, h=%f) is read (took %.3f ms)",
         __LINE__, width, height, interval);
    profiler.start();


    // Now tell the face detector to give us a list of bounding boxes
    // around all the faces in the image.
    std::vector<dlib::rectangle> dets = sFaceDetector(cimg_small, 0);
    interval = profiler.stopAndGetInterval();
    LOGI("L%d: Number of faces detected: %u (took %.3f ms)",
         __LINE__, (unsigned int) dets.size(), interval);

    // Protobuf message.
    //FaceList faces;
    // Now we will go ask the shape_predictor to tell us the pose of
    // each face we detected.
    if (!dets.empty()) {
        LOGI("L%d: face found OmG.", __LINE__);
        dlib::rectangle dets2 = dlib::rectangle(dets[0].left() * resize, dets[0].top() * resize,
                                                dets[0].right() * resize,
                                                dets[0].bottom() * resize);
        profiler.start();
        dlib::full_object_detection shape = sFaceLandmarksDetector(cimg, dets2);
        interval = profiler.stopAndGetInterval();
        LOGI("L%d: landmarks detected (took %.3f ms)",
             __LINE__, interval);

        auto *pointsMat = (cv::Mat *) pointAddress;

        for (unsigned int i = 0; i < 136; i = i + 2) {
            dlib::point &pt = shape.part(i / 2);
            //cv::Point2f point2f = cv::Point2f((float) pt.x(), (float) pt.y());

            pointsMat->at<float>(i, 0, 0) = (float) pt.x();
            pointsMat->at<float>(i + 1, 0, 0) = (float) pt.y();
        }
        //  pointsMat->at<float>(136, 0, 0) = (float) dets2.left();
        //  pointsMat->at<float>(137, 0, 0) = (float) dets2.top();
        //   pointsMat->at<float>(138, 0, 0) = (float) dets2.right();
        //   pointsMat->at<float>(139, 0, 0) = (float) dets2.bottom();

        // long *lp = (long*)malloc(sizeof(pointsMat));
        // return lp;
    } else
        LOGI("L%d: no faces found.", __LINE__);

    //return 0;
}


class PointState {
public:
    explicit PointState(cv::Point2f point)
            :
            m_point(std::move(point)),
            m_kalman(4, 2, 0, CV_64F) {
        Init();
    }

    void Update(cv::Point2f point) {
        m_point.x = point.x;   //update using measurements
        m_point.y = point.y;
        /*cv::Mat measurement(2, 1, CV_64FC1);
        if (point.x < 0 || point.y < 0)
        {
            Predict();
            measurement.at<double>(0) = m_point.x;  //update using prediction
            measurement.at<double>(1) = m_point.y;

            m_isPredicted = true;
        }
        else
        {
            measurement.at<double>(0) = point.x;  //update using measurements
            measurement.at<double>(1) = point.y;

            m_isPredicted = false;
        }

        // Correction
        cv::Mat estimated = m_kalman.correct(measurement);
        m_point.x = static_cast<float>(estimated.at<double>(0));   //update using measurements
        m_point.y = static_cast<float>(estimated.at<double>(1));

        Predict();*/
    }

    cv::Point2f GetPoint() const {
        return m_point;
    }

    bool IsPredicted() const {
        return m_isPredicted;
    }

public:
    double m_deltaTime = 0.09;

private:
    cv::Point2f m_point;
    cv::KalmanFilter m_kalman;


    double m_accelNoiseMag = 0.3;

    bool m_isPredicted = false;

    void Init() {
        m_kalman.transitionMatrix = (cv::Mat_<double>(4, 4) <<
                                                            1, 0, m_deltaTime, 0,
                0, 1, 0, m_deltaTime,
                0, 0, 1, 0,
                0, 0, 0, 1);

        m_kalman.statePre.at<double>(0) = m_point.x; // x
        m_kalman.statePre.at<double>(1) = m_point.y; // y

        m_kalman.statePre.at<double>(2) = 1; // init velocity x
        m_kalman.statePre.at<double>(3) = 1; // init velocity y

        m_kalman.statePost.at<double>(0) = m_point.x;
        m_kalman.statePost.at<double>(1) = m_point.y;

        cv::setIdentity(m_kalman.measurementMatrix);

        m_kalman.processNoiseCov = (cv::Mat_<double>(4, 4) <<
                                                           pow(m_deltaTime, 4.0) / 4.0, 0,
                pow(m_deltaTime, 3.0) / 2.0, 0,
                0, pow(m_deltaTime, 4.0) / 4.0, 0, pow(m_deltaTime, 3.0) / 2.0,
                pow(m_deltaTime, 3.0) / 2.0, 0, pow(m_deltaTime, 2.0), 0,
                0, pow(m_deltaTime, 3.0) / 2.0, 0, pow(m_deltaTime, 2.0));


        m_kalman.processNoiseCov *= m_accelNoiseMag;

        cv::setIdentity(m_kalman.measurementNoiseCov, cv::Scalar::all(0.1));

        cv::setIdentity(m_kalman.errorCovPost, cv::Scalar::all(0.1));
    }

    cv::Point2f Predict() {
        cv::Mat prediction = m_kalman.predict();
        m_point.x = static_cast<float>(prediction.at<double>(0));
        m_point.y = static_cast<float>(prediction.at<double>(1));
        return m_point;
    }
};


std::vector<cv::Point2f> newLandmarks;

cv::Mat prevGray;
std::vector<PointState> trackPoints;
std::vector<cv::Point2f> landmarks;
std::vector<cv::Point2f> prevLandmarks;

///
void TrackPoints(const cv::Mat &prevFrame, const cv::Mat &currFrame,
                 const std::vector<cv::Point2f> &currLandmarks,
                 std::vector<PointState> &trackPoints
                 ) {
    // Lucas-Kanade
    cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 30, 0.001);
    cv::Size winSize(31, 31);

    std::vector<uchar> status(68, 0);
    std::vector<float> err;

    std::vector<cv::Point2f> prevLandmarks2;
    std::for_each(trackPoints.begin(), trackPoints.end(),
                  [&](const PointState &pts) { prevLandmarks2.push_back(pts.GetPoint()); });

    cv::calcOpticalFlowPyrLK(prevFrame, currFrame, prevLandmarks2, newLandmarks, status, err,
                             winSize, 3, termcrit, cv::OPTFLOW_FARNEBACK_GAUSSIAN, 0.000001);

    for (size_t i = 0; i < status.size(); ++i) {
        //trackPoints[i].Update(newLandmarks[i]);
        if (status[i]) {

            //if (err[i] * prevFrame.rows > 10 )
            trackPoints[i].Update((newLandmarks[i]));

           // newLandmarks[i] = (6*newLandmarks[i]+currLandmarks[i])/7;
            //trackPoints[i].Update(currLandmarks[i]);

            trackPoints[i].Update((8*newLandmarks[i]+currLandmarks[i])/9);

        } else {
            trackPoints[i].Update(currLandmarks[i]);
            //trackPoints[i].Update(currLandmarks[i]);
        }
    }

    /* for (size_t i = 0; i < currLandmarks.size(); ++i)
     {
         trackPoints[i].Update(currLandmarks[i]);
     }*/


}




extern "C" JNIEXPORT void JNICALL
JNI_METHOD(initCache)(JNIEnv *env, jobject thiz) {

    // outMat = Mat(720, 1280, CV_8UC4, datas);

    trackPoints.reserve(68);
    landmarks.reserve(68);
    prevLandmarks.reserve(68);

    prevLandmarks.clear();
    prevGray.release();
    trackPoints.clear();


}

extern "C" JNIEXPORT void JNICALL
JNI_METHOD(setDeltaTime)(JNIEnv *env, jobject thiz, jdouble dTime) {

    // outMat = Mat(720, 1280, CV_8UC4, datas);
    //m_deltaTime=dTime;



}

extern "C" JNIEXPORT void JNICALL
JNI_METHOD(detectFaceDlib)(JNIEnv *env,
                           jobject thiz,
                           jlong matAddress,
                           jlong pointAddress) {
    if (sFaceDetector.num_detectors() == 0) {
        LOGI("L%d: sFaceDetector is not initialized!", __LINE__);
        //throwException(env, "sFaceDetector is not initialized!");
        // return NULL;
        return;
    }
    if (sFaceLandmarksDetector.num_parts() == 0) {
        LOGI("L%d: sFaceLandmarksDetector is not initialized!", __LINE__);
        //throwException(env, "sFaceLandmarksDetector is not initialized!");
        // return NULL;
        return;
    }

    // Profiler.
    Profiler profiler;
    profiler.start();
    // Convert bitmap to dlib::array2d.
    //dlib::array2d<dlib::bgr_pixel> img;
    //dlib::array2d<unsigned char> img;
    //convertGrayMatAddressToArray2d(env, matAddress, img);





//    // Make the image larger so we can detect small faces.
    //dlib::pyramid_up(img);
    //LOGI("L%d: pyramid_up the input image (w=%lu, h=%lu).", __LINE__, img.nc(), img.nr());

    auto *im = (cv::Mat *) matAddress;
    cv::Mat im_small, im_display;

    int resize = 4;

// Resize image for face detection
    cv::resize(*im, im_small, cv::Size(), 1.0 / resize, 1.0 / resize);




// Change to dlib's image format. No memory is copied.
    dlib::cv_image<unsigned char> cimg_small(im_small);
    dlib::cv_image<unsigned char> cimg(*im);


    const float width = (float) cimg_small.nc();
    const float height = (float) cimg_small.nr();
    double interval = profiler.stopAndGetInterval();
    LOGI("L%d: input image (w=%f, h=%f) is read (took %.3f ms)",
         __LINE__, width, height, interval);
    profiler.start();


    // Now tell the face detector to give us a list of bounding boxes
    // around all the faces in the image.
    std::vector<dlib::rectangle> dets = sFaceDetector(cimg_small, 0);
    interval = profiler.stopAndGetInterval();
    LOGI("L%d: Number of faces detected: %u (took %.3f ms)",
         __LINE__, (unsigned int) dets.size(), interval);

    // Protobuf message.
    //FaceList faces;
    // Now we will go ask the shape_predictor to tell us the pose of
    // each face we detected.
    if (!dets.empty()) {
        LOGI("L%d: face found OmG.", __LINE__);
        dlib::rectangle dets2 = dlib::rectangle(dets[0].left() * resize, dets[0].top() * resize,
                                                dets[0].right() * resize,
                                                dets[0].bottom() * resize);
        profiler.start();
        dlib::full_object_detection shape = sFaceLandmarksDetector(cimg, dets2);
        interval = profiler.stopAndGetInterval();
        LOGI("L%d: landmarks detected (took %.3f ms)",
             __LINE__, interval);

        auto *pointsMat = (cv::Mat *) pointAddress;

        for (unsigned int i = 0; i < 136/*68*/; /*i++*/i += 2) {
            dlib::point &pt = shape.part(i / 2);
            //pointsMat->at<float>(i, 0, 0) = pt.x();
            //pointsMat->at<float>(i + 1, 0, 0) = pt.y();
            landmarks.emplace_back((float) pt.x(), (float) pt.y());
        }

        if (prevGray.empty()) {
            trackPoints.clear();

            for (cv::Point2f lp : landmarks) {
                trackPoints.emplace_back(lp);
            }
        } else {
            if (trackPoints.empty()) {
                for (cv::Point2f lp : landmarks) {
                    trackPoints.emplace_back(lp);
                }
            } else {
                TrackPoints(prevGray, im_small, landmarks, trackPoints);
            }
        }

        int i = 0;
        for (const PointState &tp : trackPoints) {
            //cv::circle(frame, tp.GetPoint(), 3, tp.IsPredicted() ? cv::Scalar(0, 0, 255) : cv::Scalar(0, 255, 0), cv::FILLED);
            Point2f point = tp.GetPoint();
            pointsMat->at<float>(i, 0, 0) = point.x;
            pointsMat->at<float>(i + 1, 0, 0) = point.y;
            i += 2;
        }

        landmarks.clear();

        prevGray = im_small;
        //  pointsMat->at<float>(136, 0, 0) = (float) dets2.left();
        //  pointsMat->at<float>(137, 0, 0) = (float) dets2.top();
        //   pointsMat->at<float>(138, 0, 0) = (float) dets2.right();
        //   pointsMat->at<float>(139, 0, 0) = (float) dets2.bottom();

        // long *lp = (long*)malloc(sizeof(pointsMat));
        // return lp;
    } else
        LOGI("L%d: no faces found.", __LINE__);

    //return 0;
}

std::mutex _mutex;


extern "C" JNIEXPORT void JNICALL
JNI_METHOD(detectPoints)(JNIEnv *env,
                         jobject thiz,
                         jlong matAddress,
                         jlong pointAddress,
                         jlong tx,
                         jlong ty,
                         jlong bx,
                         jlong by) {


    Profiler profiler;

    if (sFaceDetector.num_detectors() == 0) {
        LOGI("L%d: sFaceDetector is not initialized!", __LINE__);
        //throwException(env, "sFaceDetector is not initialized!");
        // return NULL;
        return;
    }
    if (sFaceLandmarksDetector.num_parts() == 0) {
        LOGI("L%d: sFaceLandmarksDetector is not initialized!", __LINE__);
        //throwException(env, "sFaceLandmarksDetector is not initialized!");
        // return NULL;
        return;
    }

    //int res =5;

    dlib::rectangle det = dlib::rectangle(dlib::point(tx, ty), dlib::point(bx, by));

    // Profiler.


    Mat im = *((Mat *) matAddress);
    //Mat img = *im;
    //cv::Mat im_small;

// Resize image for face detection
    //cv::resize(*im, im_small, cv::Size(), 1.0/res, 1.0/res);

    profiler.start();

    //crop
    if (det.left() > 0 && det.right() < im.cols && det.top() > 0 && det.bottom() < im.rows) {
        cv::Rect faceROI(static_cast<int>(det.left()), static_cast<int>(det.top()),
                         static_cast<int>(det.width()), static_cast<int>(det.height()));
        cv::Mat face = im(faceROI);

        // apply filters
        //  cv::medianBlur(face, face, 5);  // remove noise
        // improve contrast
       // cv::equalizeHist(face, face);
    }
    double interval = profiler.stopAndGetInterval();
    LOGI("L%d: crop time (took %.3f ms)",
         __LINE__, interval);

    dlib::cv_image<unsigned char> cimg(im);

    profiler.start();
    _mutex.lock();
    dlib::full_object_detection shape = sFaceLandmarksDetector(cimg, det);
    _mutex.unlock();

    interval = profiler.stopAndGetInterval();
    LOGI("L%d: point detected time (took %.3f ms)",
         __LINE__, interval);

    auto *pointsMat = (Mat *) pointAddress;

    //Mat dottedMat = Mat(im.rows,im.cols,im.type());
    //im.copyTo(dottedMat);

    for (unsigned int i = 0; i < 136/*68*/; /*i++*/i += 2) {
        dlib::point &pt = shape.part(i / 2);
        //pointsMat->at<float>(i, 0, 0) = pt.x();
        //pointsMat->at<float>(i + 1, 0, 0) = pt.y();
        Point p = cv::Point2f(pt.x(), pt.y());
        landmarks.push_back(p);
        //dottedMat.at<uchar>(p.y, p.x)=255;

        //cv::circle(im, p, 2, cv::Scalar(0, 255, 0), cv::FILLED);


    }

    if(!prevLandmarks.empty()){
        TrackPoints(prevGray, im, landmarks, trackPoints);
    }

    if (prevGray.empty()) {
        trackPoints.clear();

        for (const cv::Point2f &lp : landmarks) {
            trackPoints.push_back(PointState(lp));
        }
    } else {
        if (trackPoints.empty()) {
            for (const cv::Point2f &lp : landmarks) {
                trackPoints.push_back(PointState(lp));
            }
        } else {
            TrackPoints(prevGray, im, landmarks, trackPoints);
        }
    }

    int i = 0;
    for (const PointState& tp : trackPoints)
    {
        //cv::circle(im, tp.GetPoint(), 2, tp.IsPredicted() ? cv::Scalar(0, 0, 255) : cv::Scalar(0, 255, 0), cv::FILLED);
        Point2f point = tp.GetPoint();
        pointsMat->at<float>(i, 0, 0) = point.x;
        pointsMat->at<float>(i + 1, 0, 0) = point.y;
        i+=2;

        //im.at<uchar>(static_cast<int>(point.y), static_cast<int>(point.x))=255;
    }

   /* for (const cv::Point2f &point : newLandmarks) {
        pointsMat->at<float>(i, 0, 0) = point.x;
        pointsMat->at<float>(i + 1, 0, 0) = point.y;
        i += 2;
    }*/

    //Store current as prev

    /*prevLandmarks.clear();
    if (newLandmarks.empty())
        std::for_each(landmarks.begin(), landmarks.end(),
                  [&](const cv::Point2f &point) { prevLandmarks.push_back(point); });
    else
        std::for_each(newLandmarks.begin(), newLandmarks.end(),
                      [&](const cv::Point2f &point) { prevLandmarks.push_back(point); });*/

    prevGray = im.clone();

    //clear all
    //newLandmarks.clear();
    landmarks.clear();



}
/*

using namespace imgUtils;

extern "C" JNIEXPORT void JNICALL JNI_METHOD(convertYUV420ToARGB8888)(
        JNIEnv* env, jclass clazz, jbyteArray y, jbyteArray u, jbyteArray v,
        jintArray output, jint width, jint height, jint y_row_stride,
        jint uv_row_stride, jint uv_pixel_stride, jboolean halfSize) {


    jboolean inputCopy = JNI_FALSE;
    jbyte* const y_buff = env->GetByteArrayElements(y, &inputCopy);
    jboolean outputCopy = JNI_FALSE;
    jint* const o = env->GetIntArrayElements(output, &outputCopy);


    if (halfSize) {
        ConvertYUV420SPToARGB8888HalfSize(reinterpret_cast<uint8*>(y_buff),
                                          reinterpret_cast<uint32*>(o), width,
                                          height);
    } else {
        jbyte* const u_buff = env->GetByteArrayElements(u, &inputCopy);
        jbyte* const v_buff = env->GetByteArrayElements(v, &inputCopy);

        */
/*ConvertYUV420ToARGB8888(
                reinterpret_cast<uint8*>(y_buff), reinterpret_cast<uint8*>(u_buff),
                reinterpret_cast<uint8*>(v_buff), reinterpret_cast<uint8*>(o), width,
                height, y_row_stride, uv_row_stride, uv_pixel_stride);*//*


        env->ReleaseByteArrayElements(u, u_buff, JNI_ABORT);
        env->ReleaseByteArrayElements(v, v_buff, JNI_ABORT);
    }

    env->ReleaseByteArrayElements(y, y_buff, JNI_ABORT);
    env->ReleaseIntArrayElements(output, o, 0);
}

*/
/*extern "C" JNIEXPORT void JNICALL JNI_METHOD(cvtYUV420ToARGB8888)(
        JNIEnv* env, jclass clazz, long inMatAddress,
        long outMatAddress, jint width, jint height, jint y_row_stride,
        jint uv_row_stride, jint uv_pixel_stride, jboolean halfSize) {


    //jboolean inputCopy = JNI_FALSE;
    //jbyte* const y_buff = env->GetByteArrayElements(y, &inputCopy);
    //jboolean outputCopy = JNI_FALSE;
    //jint* const o = env->GetIntArrayElements(output, &outputCopy);
    auto *inMat = (cv::Mat *) inMatAddress;
    auto *outMat = (cv::Mat *) outMatAddress;


    if (halfSize) {
        ConvertYUV420SPToARGB8888HalfSize(reinterpret_cast<uint8*>(y_buff),
                                          reinterpret_cast<uint32*>(o), width,
                                          height);
    } else {
        jbyte* const u_buff = env->GetByteArrayElements(u, &inputCopy);
        jbyte* const v_buff = env->GetByteArrayElements(v, &inputCopy);

        ConvertYUV420ToARGB8888(
                reinterpret_cast<uint8*>(y_buff), reinterpret_cast<uint8*>(u_buff),
                reinterpret_cast<uint8*>(v_buff), reinterpret_cast<uint8*>(o), width,
                height, y_row_stride, uv_row_stride, uv_pixel_stride);

        env->ReleaseByteArrayElements(u, u_buff, JNI_ABORT);
        env->ReleaseByteArrayElements(v, v_buff, JNI_ABORT);
    }

    env->ReleaseByteArrayElements(y, y_buff, JNI_ABORT);
    env->ReleaseIntArrayElements(output, o, 0);
}*//*

static const int kMaxChannelValue = 262143;
static inline uint32 YUV2RGB(int nY, int nU, int nV) {
    nY -= 16;
    nU -= 128;
    nV -= 128;
    if (nY < 0)
        nY = 0;

    // This is the floating point equivalent. We do the conversion in integer
    // because some Android devices do not have floating point in hardware.
    // nR = (int)(1.164 * nY + 2.018 * nU);
    // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
    // nB = (int)(1.164 * nY + 1.596 * nV);

    int nR = (int) (1192 * nY + 1634 * nV);
    int nG = (int) (1192 * nY - 833 * nV - 400 * nU);
    int nB = (int) (1192 * nY + 2066 * nU);

    nR = MIN(kMaxChannelValue, MAX(0, nR));
    nG = MIN(kMaxChannelValue, MAX(0, nG));
    nB = MIN(kMaxChannelValue, MAX(0, nB));

    nR = (nR >> 10) & 0xff;
    nG = (nG >> 10) & 0xff;
    nB = (nB >> 10) & 0xff;

    return 0xff000000 | (nR << 16) | (nG << 8) | nB;
}

using namespace cv;
static unsigned char datas[1280*720*4];
static Mat mat;
int a;

void convertYUV420_888_to_RGB2(cv::Mat *in,cv::Mat *out,int width,int height) {
    uchar *data = in->data;
    uchar *outData = out->data;
    int size = width * height;
    //unsigned char datas[1280*720*4];


    //Profiler profiler;


    //IplImage* img1 = cvCreateImage(cvGetSize(origImage), IPL_DEPTH_8U, 3);
    Vec4b* ptr;
    int row;
    int col;
    //Mat lookUpTable(height, width, CV_8UC4);
    //uchar* p = lookUpTable.data;

    for (int i = 0; i < size; i++) {
        uchar Y = data[i];
        uchar U = data[size + (i / 2)];
        uchar V = data[size + (size / 2) + (i / 2)];//was 1.25
        */
/*

        int nY = Y- 16;
        int nU = U- 128;
        int nV = V- 128;
        if (nY < 0)
            nY = 0;

        // This is the floating point equivalent. We do the conversion in integer
        // because some Android devices do not have floating point in hardware.
        // nR = (int)(1.164 * nY + 2.018 * nU);
        // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
        // nB = (int)(1.164 * nY + 1.596 * nV);

        int nR = (int) (1192 * nY + 1634 * nV);
        int nG = (int) (1192 * nY - 833 * nV - 400 * nU);
        int nB = (int) (1192 * nY + 2066 * nU);

        nR = MIN(kMaxChannelValue, MAX(0, nR));
        nG = MIN(kMaxChannelValue, MAX(0, nG));
        nB = MIN(kMaxChannelValue, MAX(0, nB));

        nR = (nR >> 10) & 0xff;
        nG = (nG >> 10) & 0xff;
        nB = (nB >> 10) & 0xff;
*//*


        //return ;
        */
/*float R = Y + 1.402f * (V - 128);
        float G = Y - 0.344f * (U - 128) - 0.714 * (V - 128);
        float B = Y + 1.772f * (U - 128);


        if (R < 0) { R = 0; }
        if (G < 0) { G = 0; }
        if (B < 0) { B = 0; }
        if (R > 255) { R = 255; }
        if (G > 255) { G = 255; }
        if (B > 255) { B = 255; }*//*



        //profiler.start();
        //out->at<unsigned int>(i/height,i/width)=a;
        //outData[i*4]=0xff000000 | (nR << 16) | (nG << 8) | nB;
        //row = out->rows;
        //col = out->cols;
        //ptr = out->ptr<Vec4b>(row);
        //ptr[col] = Vec4b(nR, nG, nB,0xFF);
        datas[i*4] = Y;
        datas[i*4+1] = U;
        datas[i*4+2] = V;
        datas[i*4+3] = 0xFF;
        a=5;
        //double interval = profiler.stopAndGetInterval();
        //LOGI("L%d: set time (took %.4f ms)",
        //     __LINE__, interval);
        // out->at<unsigned char>(i/height,i/width,0)=(unsigned char)R;
        // out->at<unsigned char>(i/height,i/width,1)=(unsigned char)G;
        // out->at<unsigned char>(i/height,i/width,2)=(unsigned char)B;
        // out->at<unsigned char>(i/height,i/width,3)=0xFF;

    }
    //LUT(I, lookUpTable, J);
}





extern "C" JNIEXPORT void JNICALL
JNI_METHOD(covertYUV2RGB)(JNIEnv *env,
                         jobject thiz,
                         jlong inMatAddress,
                         jlong outMatAddress,
                         jint width,
                         jint height) {



    Profiler profiler;
    profiler.start();

    _mutex.lock();

    Mat *in = ((Mat *) inMatAddress);
    Mat *out = ((Mat *) outMatAddress);


    convertYUV420_888_to_RGB2(in,out,width,height);

    _mutex.unlock();

    double interval = profiler.stopAndGetInterval();
    LOGI("L%d: convert time (took %.3f ms) %d",
         __LINE__, interval,datas[0]+a);
}


*/



